const express = require('express')
require('dotenv').config()
const session = require('express-session')
const server = express()
let port = process.env.PORT
const oneDay = 1000 * 60 * 60 * 24;
const bodyParser = require('body-parser')
const passport = require('passport')
const flash = require('connect-flash')
const cookieParser = require('cookie-parser');
var morgan = require('morgan')
const ModelDashboard = require('./src/model/dashboard')
require('./config/passport')(passport)

const user = require('./src/routers/user')
const setting = require('./src/routers/setting')
const manageOrder = require('./src/routers/manageOrder')
const api = require('./src/routers/api')
const product = require('./src/routers/product')
const epayment = require('./src/routers/epayment')
const shop = require('./src/routers/shop')
const creativestore = require('./src/routers/creativestore')
const warehouse = require('./src/routers/wh')
const {oneoms,whoms,lish_member} = require('./config/Database')

const isLoggedIn = async(req, res, next) => {
    if (req.isAuthenticated()) {
        req.app.locals.username = req.user.email
        req.app.locals.tel = req.user.tel
        req.app.locals.type = req.user.type_sent
        req.app.locals.data = req.user
        req.app.locals.first_name = req.user.first_name
        req.app.locals.last_name = req.user.last_name
        req.app.locals.status = req.user.status_order
        req.app.locals.points = req.user.points
        req.session.count = 0;
        if(req.user.shop_code == null){
            req.app.locals.shop = 0
            req.app.locals.image_pf = '/assets/img/90x90.jpg'
            req.app.locals.shop_name = `ชื่อร้านค้า`
        }else{
            let img_data = await lish_member.query(`SELECT * FROM setting_shop where shop_code = '${req.user.shop_code}'`)
            if(img_data.rows.length == 0){
                req.app.locals.shop = ''
                req.app.locals.image_pf = '/assets/img/90x90.jpg'
                req.app.locals.image_bg = '/assets/img/90x90.jpg'
                req.app.locals.shop_name = `ชื่อร้านค้า`
            }else{
                req.app.locals.shop = req.user.shop_code
                req.app.locals.shop_id = img_data.rows[0].id
                req.app.locals.shop_name = img_data.rows[0].shop_name
                req.app.locals.image_pf = 'http://128.199.100.251:1001' + img_data.rows[0].image_profile
                req.app.locals.image_bg = 'http://128.199.100.251:1001' + img_data.rows[0].image_bg
            }
        }
        next();
    } else {
        console.log(1)
        res.redirect('/login');
    }
};

server.use(express.json())
server.set("views","./src/views")
server.set("view engine","ejs")
server.use(express.static('./src/public/'))
// server.use(morgan('combined'))
server.use(session({
    secret: "lishmenber_client",
    saveUninitialized: true,
    cookie: { maxAge: oneDay },
    resave: false
}));
server.use(bodyParser.urlencoded({
    extended: true
}))
server.use(cookieParser());
server.use(bodyParser.json())
server.use(flash())
server.use(passport.initialize())
server.use(passport.session())

server.post('/auth',passport.authenticate('login',{
    failureRedirect: '/login',
    successRedirect: '/',
    failureFlash: true
    })
);

server.get('/',isLoggedIn,async (req,res)=>{
    let ModelQuery = new ModelDashboard.Dashbord()
    let realtotal = await ModelQuery.realtotal(req.user.shop_code)
    let realChannel = await ModelQuery.realChannel(req.user.shop_code)
    res.render('index',{data:realtotal,channel:realChannel.rows})
})

server.get('/Login',(req,res)=>{
    let message = req.flash('error');
    res.render('Login',{message:message})
})

server.get('/logout',(req,res)=>{
    req.session.destroy();
    res.redirect('/login')
})

// server.use(function(req, res, next) {
//     throw new Error('BROKEN1') // กำหนด error เอง
// })

server.use('/image', express.static('image'));
server.use('/warehouse',isLoggedIn,warehouse)
server.use('/epayment',isLoggedIn,epayment)
server.use('/Product',isLoggedIn,product)
server.use('/Register',user)
server.use('/Shop',shop)
server.use('/Manageorder',isLoggedIn,manageOrder)
server.use('/Setting',isLoggedIn,setting)
server.use('/Creativestore',isLoggedIn,creativestore)
server.use('/api',api)

server.listen(port,()=>{
    console.log('test in port : '+port)
})