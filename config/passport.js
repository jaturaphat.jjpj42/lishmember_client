const bcrypt= require('bcryptjs')
const {oneoms,whoms,lish_member} = require('./Database.js')
var LocalStrategy = require('passport-local').Strategy

module.exports = function(passport) {
    passport.serializeUser(function(user, done) {
        done(null, user);
    });
    passport.deserializeUser(function(user, done) {
        done(null, user);
    });

    passport.use('login', new LocalStrategy({
        passReqToCallback : true
    },function(req, username, password, done) {
        loginUser()
        async function loginUser() {
            const client = lish_member
            try {
                var accData = await JSON.stringify(await client.query('SELECT * FROM "member_lish" u WHERE "email" = $1', [username],async (err, result) => {
                    if (err) {
                            return done(err)
                    }
                    if (result.rows[0] == null) {
                        return done(null, false, {message: "Incorrect username or password"})
                    }else if(result.rows[0].status == 0){
                        return done(null, false, {message: "Accout is can't Login"})
                    }else{
                        bcrypt.compare(password, result.rows[0].password,async (err, valid) => {
                            if (err) {
                                console.log("Error on password validation")
                                return done(err)
                            }
                            if (valid) {
                                client.query('update "member_lish" set login_latest = now() WHERE "email" = $1', [username])
                                // console.log(`select * from member_order where email = '${result.rows[0].email}' and order_status = 2`)
                                let check = await lish_member.query(`select * from member_order where email like '%${result.rows[0].email}%' and order_status = 2`)
                                if(check.rows.length == 0){
                                    result.rows[0].status_order = 0
                                    return done(null, result.rows[0])
                                }else{
                                    console.log('User [' + req.body.username + '] has logged in.')
                                    result.rows[0].status_order = 1
                                    return done(null, result.rows[0])
                                }
                            } else {
                                return done(null, false, {message:"Incorrect username or password"})
                            }
                        })        
                    }
                    })
                )
            }catch(e) {
                throw (e)
            }
        }
    }))

    passport.use('register', new LocalStrategy({
        usernameField : 'username',
        passwordField : 'password',
        passReqToCallback : true
    },
    function(req, username, password, done) {
        registerUser()
        async function registerUser() {
            // const client = await pool.connect()
            try {
                let passHash = await bcrypt.hash(req.body.password, 8)
                await JSON.stringify(pool.query('SELECT "users_id" FROM users WHERE users_name=($1)', [req.body.username], (err, result) => {
                    if (err) {
                        return done(err)
                    }
                        if (result.rows[0]) {
                            return done(null, false, req.flash('message', 'Sorry, this username is already taken.'))
                        } else {
                            let { firstname, lastname, telephone, email ,imglink} = req.body
                            const {format} = require('date-fns')
                            pool.query('INSERT INTO users (users_name, password, firstname, lastname, image, email, telephone, created_at) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)', [username, passHash,firstname,lastname,imglink,email,telephone,format(new Date(), 'MM-dd-yyyy H:m:s')], (err, result) => {
                                if (err) {
                                    console.log(err)
                                }
                                else {
                                    pool.query('COMMIT')
                                    console.log('User [' + req.body.username + '] has registered.')
                                    //console.log(result)
                                    return done(null, {username: req.body.username})
                                }
                            });
                        }
                }))
            }
            catch(e) {
                throw (e)
            }
        }
    }))

    passport.use('updatePassword', new LocalStrategy({
        usernameField : 'password',
        passwordField : 'newpass',
        passReqToCallback : true
    },
    function(req, password, newpass, done) {
        let username = (req.user.username.users_name)
        console.log(req.user.username.users_name)
        updatePassword()
        async function updatePassword() {
            // const client = await pool.connect()
            try {
                await pool.query('BEGIN')
                let newPassHash = await bcrypt.hash(req.body.newpass, 8)
                var accData = await JSON.stringify(pool.query('SELECT "users_id", "users_name", "password" FROM "users" WHERE "users_name"=$1', [username], (err, result) => {
                    if (err) {
                        return done(err)
                    }
                    if(result.rows[0] == null) {
                            return done(null, false, req.flash('message', 'Error on changing password. Please try again'))
                    } else {
                            bcrypt.compare(req.body.password, result.rows[0].password, (err, valid) => {
                            if (err) {
                                console.log("Error on current password validation")
                                return done(err)
                            }
                            if (valid) {
                                pool.query('UPDATE users SET password=($1) WHERE users_name=($2)', [newPassHash,username], (err, result) => {
                                    if (err) {
                                        console.log(err)
                                    }
                                    else {
                                        pool.query('COMMIT')
                                        console.log('User [' + username + '] has updated their password.')
                                        return done(null, {username: req.user.username}, req.flash('message', 'Your password has been updated.'))
                                    }
                                });
                            } else {
                                req.flash('message', "Incorrect current password entered")
                                return done(null, false)
                            }
                        })
                    }
                }))
            }
            catch(e) {
                throw (e)
            }
        }
    }))

    passport.use('forgotPassword', new LocalStrategy({
        passReqToCallback : true,
    },
    function(req, password, newpass, done) {
        forgotPassword()
        async function forgotPassword() {
            try {
                let newPassHash = await bcrypt.hash(req.body.password, 8)
                var accData = await JSON.stringify(pool.query('SELECT "users_name", "password" FROM "users" WHERE "users_name"=$1', [req.body.username.toLowerCase()], (err, result) => {
                    if (err) {
                        return done(err)
                    }
                    if(result.rows[0] == null) {
                        return done(null, false, req.flash('message', 'Error on changing password. Please try again'))
                    } else {
                        if (req.body.password != req.body.confirmpass) {
                            return done(null, false, req.flash('message', 'Sorry, Password don`t match.'))
                        } else {
                            let { password } = req.body
                            const {format} = require('date-fns')
                            pool.query('UPDATE users SET password=($1) WHERE users_name=($2)', [newPassHash, req.body.username], (err, result) => {
                                if (err) {
                                    console.log(err)
                                }
                                else {
                                    pool.query('COMMIT')
                                    console.log('User [' + req.body.username + '] has registered.')
                                    return done(null, {username: req.body.username})
                                }
                            });
                        }
                    }
                }))
            }
            catch(e) {
                throw (e)
            }
        }
    }))

}

