const Pool = require('pg').Pool
require('dotenv').config()

const oneoms = new Pool({
	user: process.env.user,
	host: process.env.host,
	database: process.env.dboms,
	password: process.env.password,
	port: process.env.port_db,
	ssl:{
		rejectUnauthorized: false, 
	}
});

const whoms = new Pool({
	user: process.env.user,
	host: process.env.host,
	database: process.env.dbwh,
	password: process.env.password,
	port: process.env.port_db,
	ssl:{
		rejectUnauthorized: false, 
	}
});

const lish_member = new Pool({
	user: process.env.user,
	host: process.env.host,
	database: process.env.dblm,
	password: process.env.password,
	port: process.env.port_db,
	ssl:{
		rejectUnauthorized: false, 
	}
});

oneoms.connect()
whoms.connect()
lish_member.connect()

async function select_lish(commands){
	let data = await lish_member.query(commands)
	return(data.rows)
}

module.exports = {oneoms,whoms,lish_member,select_lish}