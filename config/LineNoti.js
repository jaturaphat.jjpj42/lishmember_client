const request = require('request');
require('dotenv').config()
// const LINE_NOTIFY = process.env.LINE_NOTIFY

function SendNoti(message,room, callback) {
    request({
        method: 'POST',
        uri: `https://notify-api.line.me/api/notify`,
        header: {
            'Content-Type': 'multipart/form-data',
        },
        auth: {
            bearer: room,
        },
        form: {
            message: message,
            // stickerPackageId:message[1],
            // stickerId:message[2]
        },
    }, (err, httpResponse, body) => {
        if (err) {
            callback(err , false)
        } else {
            callback(null , true)
        }
    })
}

module.exports = SendNoti