

const  swalTwoSubmit = (textfirst) => {
   return Swal.fire({
        title: textfirst,
        showCancelButton: true,
        confirmButtonText: 'ยืนยัน',
        cancelButtonText: `ยกเลิก`,
      })
} 


const swalRedirect = (icon,textsecond,redirectUrl) =>{
    return Swal.fire({
        icon: icon,
        title: textsecond,
        }).then((resulttwo)=>{
        if(resulttwo.isConfirmed){
            window.location = redirectUrl;
        }
    })
}