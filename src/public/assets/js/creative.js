const items = document.querySelectorAll('.list-group')
const columns = document.querySelectorAll('.column')
let dragItem = null;
let j = 1;
let vdo = 1;
var total = 0;
items.forEach(item=>{
    item.addEventListener('dragstart',dragStart)
    item.addEventListener('dragend', dragEnd);
})

columns.forEach(column =>{
    column.addEventListener('dragover', dragOver);
    column.addEventListener('dragenter', dragEnter);
    column.addEventListener('dragleave', dragLeave);
    column.addEventListener('drop', dragDrop);
})

columns.forEach(async (column) => {
    new Sortable(column, {
        animation: 150,
        ghostClass: "blue-background-class",
        onEnd: function (evt){
            changeId();
        }
    })
});

function dragStart(){
    dragItem = this
    //console.log(dragItem);
}
function dragEnd(){
    dragItem = null;
    let photonumber = 0
    let col = document.querySelector('.column');
    let number = col.querySelectorAll('.list-group-item')

    /* else{
        $("#photoslide").attr("draggable","true")
        $("#photoslide").addClass("list-group")
        $("#photoslide").removeClass("list-group-disabled")
        $("#phslide").removeAttr("draggable")
    }   */
    if(number.length >= 15){
        $("div.list-group").removeAttr('draggable')
        $("div.list-group").addClass("list-group-disabled")
        $("div.list-group").removeClass("list-group")
        $(".card-img-top").attr('draggable','false')
    }else{
        $("div.list-group-disabled").addClass("list-group")
        $("div.list-group").removeClass("list-group-disabled")
        $("div.list-group").attr('draggable','true')
        $(".card-img-top").removeAttr('draggable')
    }
    for(let i = 0 ; i < number.length;i++){
        if(number[i].id === `photoslide${i+1}`){
            photonumber +=1
        }
    }
    if(total >= 3){
        $("#photoslide").removeAttr("draggable")
        $("#photoslide").addClass("list-group-disabled")
        $("#photoslide").removeClass("list-group")
        $("#phslide").attr("draggable","false")
    }
    if(photonumber >= 3){
        $("#photoslide").removeAttr("draggable")
        $("#photoslide").addClass("list-group-disabled")
        $("#photoslide").removeClass("list-group")
        $("#phslide").attr("draggable","false")
    }
}
function dragOver(e){
    e.preventDefault();
    console.log("drag over");
}
function dragEnter(){
    console.log("drag enter");
}
function dragLeave(){
    console.log("drag leave");
}
function dragDrop(){
    //console.log(dragItem.id);
    let col = document.querySelector('.column');
    let number = col.querySelectorAll('.list-group-item')
    if(dragItem.id == 'photoslide'){
        total+=1
        console.log(total);
        let div = document.createElement('div');
            div.className = 'list-group-item'
            div.id = `photoslide${number.length + 1}`
            div.style = 'width:auto;height:auto;'
        let div_row = document.createElement('div');
            div_row.className = 'row';
        let span_txt = document.createTextNode("\u00D7");
        let h5 = document.createElement('h5')
            h5.appendChild(document.createTextNode(`ใส่รูปภาพสไลด์`))
        let close = document.createElement('span');
            close.className = 'close btn-danger'
            close.classList.add('btn')
            close.appendChild(span_txt)
            div.appendChild(close)
            div.appendChild(h5);
            div.setAttribute('draggable','true')
            for(let i= 0 ; i < 5;i++){
                let div_col = document.createElement('div');
                div_col.className = 'mt-4 col'
                let input = document.createElement('input')
                input.classList.add('dropify')
                input.style = 'padding:10px;'
                input.type = "file" 
                input.id = `${number.length + 1}imgSlide${i+1}`
                div_col.appendChild(input);
                div_row.appendChild(div_col)
            }
            div.appendChild(div_row)
        close.addEventListener('click',()=>{
            this.removeChild(div)
            let count = col.querySelectorAll('.list-group-item')
            let countInput = col.querySelectorAll('input')
            for( let k = 0; k < count.length; k++ ){
                if(count[k].id.indexOf(`photoslide`) != -1){
                    count[k]. id = `photoslide${parseInt(k) + 1}`
                    let info = count[k].querySelectorAll('input')
                    for(let i = 0; i< info.length; i++){
                        info[i].id = `${k+1}imgSlide${i+1}`
                    }
                }else{
                    changeId(count[k].id);
                }
                
            }
            if(count.length == 0){
                $('#pre').attr('disabled','true')
            }
            total -=1;
            dragEnd()
        })
        this.appendChild(div)
    }else if(dragItem.id == 'clip'){
        let div = document.createElement('div');
            div.className = 'list-group-item'
            div.id = `video${number.length + 1}`
            div.style = 'width:auto;height:auto;'
        let span_txt = document.createTextNode("\u00D7");
        let h5 = document.createElement('h5')
            h5.appendChild(document.createTextNode(`ใส่ลิ้งค์วิดีโอ`))
        let close = document.createElement('span');
            close.className = 'close btn-danger'
            close.classList.add('btn')
            close.appendChild(span_txt)
            div.appendChild(close)
            div.appendChild(h5);
        let input = document.createElement('input')
            input.classList.add('form-control')
            input.classList.add('mt-5')
            input.style = 'padding:10px;'
            input.type = "url" 
            input.id = `vdo${number.length + 1}`
            input.setAttribute('placeholder','<iframe> </iframe>')
            div.setAttribute('draggable','true')
            div.appendChild(input);
        close.addEventListener('click',()=>{
            this.removeChild(div)
            let count = col.querySelectorAll('.list-group-item')
            let countInput = col.querySelectorAll('input')
            let total =1
            for( let k = 0; k < count.length; k++ ){
                if(count[k].id.indexOf(`video`) != -1){
                    count[k]. id = `video${parseInt(k) + 1}`
                    let info = count[k].querySelectorAll('input')
                    for(let i = 0; i< info.length; i++){
                        info[i].id = `vdo${total}`
                    }
                    total++;
                }else{
                    changeId(count[k].id);
                }
            }
            if(count.length == 0){
                $('#pre').attr('disabled','true')
            }
            dragEnd()
        })
        this.appendChild(div)
    }else if(dragItem.id == 'photo'){
        let div = document.createElement('div');
            div.className = 'list-group-item'
            div.id = `onlyphoto${number.length + 1}`
            div.style = 'width:auto;height:auto;'
        let span_txt = document.createTextNode("\u00D7");
        let h5 = document.createElement('h5')
            h5.appendChild(document.createTextNode(`ใส่รูปภาพ`))
        let close = document.createElement('span');
            close.className = 'close btn-danger'
            close.classList.add('btn')
            close.appendChild(span_txt)
            div.appendChild(close)
            div.appendChild(h5);
        let div_col = document.createElement('div');
        let input = document.createElement('input')
            input.className = 'mt-4'
            input.classList.add('dropify')
            input.style = 'padding:10px;'
            input.type = "file" 
            input.id = `img${number.length + 1}`
            div.setAttribute('draggable','true')
            div_col.appendChild(input);
            div.appendChild(div_col)
        close.addEventListener('click',()=>{
            this.removeChild(div)
            let count = col.querySelectorAll('.list-group-item')
            let countInput = col.querySelectorAll('input')
            let total =1
            for( let k = 0; k < count.length; k++ ){
                //console.log(count[k].id.indexOf(`photo`));
                if(count[k].id.indexOf(`onlyphoto`) != -1){
                    count[k]. id = `onlyphoto${parseInt(k) + 1}`
                    let info = count[k].querySelectorAll('input')
                    for(let i = 0; i< info.length; i++){
                        info[i].id = `img${total}`
                    }
                    total++;
                }else{
                    changeId(count[k].id);
                }
            }
            if(count.length == 0){
                $('#pre').attr('disabled','true')
            }
            dragEnd()
        })
        this.appendChild(div)
    }else if(dragItem.id == '2photo'){
        let div = document.createElement('div');
            div.className = 'list-group-item'
            div.id = `secondphoto${number.length + 1}`
            div.style = 'width:auto;height:auto;'
        let div_row = document.createElement('div');
            div_row.className = 'row';
        let span_txt = document.createTextNode("\u00D7");
        let h5 = document.createElement('h5')
            h5.appendChild(document.createTextNode(`ใส่รูปภาพ 2 รูป`))
        let close = document.createElement('span');
            close.className = 'close btn-danger'
            close.classList.add('btn')
            close.appendChild(span_txt)
            div.appendChild(close)
            div.appendChild(h5);
        for(let i=0; i < 2;i++){
            let div_col = document.createElement('div');
                div_col.className = 'mt-4 col'
            let input_img = document.createElement('input')
                input_img.classList.add('dropify')
                input_img.style = 'padding:10px;'
                input_img.type = "file" 
                input_img.id = `${number.length + 1}imgsecond${i+1}`
            div_col.appendChild(input_img);
            div_row.appendChild(div_col)
        }
            div.appendChild(div_row)
        close.addEventListener('click',()=>{
            this.removeChild(div)
            let count = col.querySelectorAll('.list-group-item')
            let countInput = col.querySelectorAll('input')
            for( let k = 0; k < count.length; k++ ){
                if(count[k].id.indexOf(`secondphoto`) != -1){
                    count[k]. id = `secondphoto${parseInt(k) + 1}`
                    let info = count[k].querySelectorAll('input')
                    for(let i = 0; i< info.length; i++){
                        info[i].id = `${k+1}imgsecond${i+1}`
                    }
                }else{
                    changeId(count[k].id);
                }
                
            }
            if(count.length == 0){
                $('#pre').attr('disabled','true')
            }
            dragEnd()
        })
        this.appendChild(div)
    }else if(dragItem.id == 'photolink'){
        let div = document.createElement('div');
            div.className = 'list-group-item'
            div.id = `Urlphoto${number.length + 1}`
            div.style = 'width:auto;height:auto;'
        let span_txt = document.createTextNode("\u00D7");
        let h5 = document.createElement('h5')
            h5.appendChild(document.createTextNode(`ใส่รูปภาพและลิ้งค์`))
        let close = document.createElement('span');
            close.className = 'close btn-danger'
            close.classList.add('btn')
            close.appendChild(span_txt)
            div.appendChild(close)
            div.appendChild(h5);
        let div_col = document.createElement('div');
            div_col.className = 'mt-5'
        let input = document.createElement('input')
            input.className = 'mt-4'
            input.classList.add('dropify')
            input.style = 'padding:10px;'
            input.type = "file" 
            input.id = `imglink${number.length + 1}`
        let input_url = document.createElement('input')
            input_url.className = 'mt-2'
            input_url.classList.add('form-control')
            input_url.style = 'padding:10px;'
            input_url.type = "url"
            input_url.id = `imgUrl${number.length + 1}`
            input_url.setAttribute('placeholder','Url')
            div.setAttribute('draggable','true')
            div_col.appendChild(input);
            div_col.appendChild(input_url);
            div.appendChild(div_col)
        close.addEventListener('click',()=>{
            this.removeChild(div)
            let count = col.querySelectorAll('.list-group-item')
            let countInput = col.querySelectorAll('input')
            for(let k = 0 ; k < count.length ; k++){
                if(count[k].id.indexOf('Urlphoto') != -1){
                    let info = count[k].querySelectorAll('input')
                    count[k].id = `Urlphoto${k+1}`
                    for(let i = 0 ; i < info.length ; i++){
                        if(info[i].id.indexOf(`imglink`) != -1){
                            info[i].id = `imglink${k+1}`
                        }else if(info[i].id.indexOf(`imgUrl`)!= -1){
                            info[i].id = `imgUrl${k+1}`
                        }
                    }
                }else{
                    changeId(count[k].id)
                }
            }
            if(count.length == 0){
                $('#pre').attr('disabled','true')
            }
            dragEnd()
        })
        this.appendChild(div)
    }else if(dragItem.id == 'photo2link'){
        let div = document.createElement('div');
            div.className = 'list-group-item'
            div.id = `photosecondUrl${number.length + 1}`
            div.style = 'width:auto;height:auto;'
        let div_row = document.createElement('div');
            div_row.className = 'row';
            let span_txt = document.createTextNode("\u00D7");
            let h5 = document.createElement('h5')
            h5.appendChild(document.createTextNode(`ใส่รูปภาพ2รูปและลิ้งค์`))
            let close = document.createElement('span');
            close.className = 'close btn-danger'
            close.classList.add('btn')
            close.appendChild(span_txt)
            div.appendChild(close)
            div.appendChild(h5);
            div.setAttribute('draggable','true')
            for(let i= 0; i < 2;i++){
                let div_col = document.createElement('div');
                    div_col.className = 'mt-4 col'
                let input_img = document.createElement('input')
                    input_img.classList.add('dropify')
                    input_img.style = 'padding:10px;'
                    input_img.type = "file" 
                    input_img.id = `${number.length + 1}imgsecondlink${i+1}`
                let input_url = document.createElement('input')
                    input_url.className = 'mt-2'
                    input_url.classList.add('form-control')
                    input_url.style = 'padding:10px;'
                    input_url.type = "url"
                    input_url.id = `${number.length + 1}img2Url${i+1}`
                    input_url.setAttribute('placeholder',`Url${i+1}`)
                div_col.appendChild(input_img);
                div_col.appendChild(input_url);
                div_row.appendChild(div_col)
            }
            div.appendChild(div_row)
        close.addEventListener('click',()=>{
            this.removeChild(div)
            let sum = 1;
            let count = col.querySelectorAll('.list-group-item')
            let countInput = col.querySelectorAll('input')
            for(let k = 0 ; k < count.length ; k++){
                if(count[k].id.indexOf('photosecondUrl') != -1){
                    let info = count[k].querySelectorAll('input.dropify')
                    let infourl = count[k].querySelectorAll('input[type="url"]')
                    count[k].id = `photosecondUrl${k+1}`
                    for(let i = 0 ; i < info.length ; i++){
                        info[i].id = `${sum}imgsecondlink${i+1}`
                    }
                    for(let i = 0 ; i < infourl.length ; i++){
                        infourl[i].id = `${sum}img2Url${i+1}`
                    }
                    sum++;
                }else{
                    changeId(count[k].id)
                }
            }
            if(count.length == 0){
                $('#pre').attr('disabled','true')
            }
            dragEnd()
        })
        this.appendChild(div)
    }else if (dragItem.id == 'message'){
        let div = document.createElement('div');
            div.className = 'list-group-item'
            div.id = `Message${number.length + 1}`
            div.style = 'width:auto;height:auto;'
        let span_txt = document.createTextNode("\u00D7");
        let h5 = document.createElement('h5')
            h5.appendChild(document.createTextNode(`ใส่ข้อความ`))
        let close = document.createElement('span');
            close.className = 'close btn-danger'
            close.classList.add('btn')
            close.appendChild(span_txt)
            div.appendChild(close)
            div.appendChild(h5);
        let input = document.createElement('textarea')
            input.classList.add('form-control')
            input.classList.add('mt-5')
            input.style = 'padding:10px;'
            input.type = "text" 
            input.setAttribute('placeholder','กรอกข้อความ')
            input.id = `text${number.length + 1}`
            div.setAttribute('draggable','true')
            div.appendChild(input);
        close.addEventListener('click',()=>{
            this.removeChild(div)
            let count = col.querySelectorAll('.list-group-item')
            let countInput = col.querySelectorAll('input')
            let total =1
            for( let k = 0; k < count.length; k++ ){
                if(count[k].id.indexOf(`Message`) != -1){
                    count[k]. id = `Message${parseInt(k) + 1}`
                    let info = count[k].querySelectorAll('input')
                    for(let i = 0; i< info.length; i++){
                        info[i].id = `text${total}`
                    }
                    total++;
                }else{
                    changeId(count[k].id);
                }
            }
            if(count.length == 0){
                $('#pre').attr('disabled','true')
            }
            dragEnd()
        })
        this.appendChild(div)
    }else if(dragItem.id == 'Line'){
        let div = document.createElement('div');
            div.className = 'list-group-item'
            div.id = `Line${number.length + 1}`
            div.style = 'width:auto;height:auto;'
        let span_txt = document.createTextNode("\u00D7");
        let h5 = document.createElement('h5')
            h5.appendChild(document.createTextNode(`ใส่ลิ้งค์ Line`))
        let close = document.createElement('span');
            close.className = 'close btn-danger'
            close.classList.add('btn')
            close.appendChild(span_txt)
            div.appendChild(close)
            div.appendChild(h5);
        let input = document.createElement('input')
            input.classList.add('form-control')
            input.classList.add('mt-5')
            input.style = 'padding:10px;'
            input.type = "text" 
            input.setAttribute('placeholder','UrlLine')
            input.id = `addLine${number.length + 1}`
            div.setAttribute('draggable','true')
            div.appendChild(input);
        close.addEventListener('click',()=>{
            this.removeChild(div)
            let count = col.querySelectorAll('.list-group-item')
            let countInput = col.querySelectorAll('input')
            let total =1
            for( let k = 0; k < count.length; k++ ){
                if(count[k].id.indexOf(`Line`) != -1){
                    count[k]. id = `Line${parseInt(k) + 1}`
                    let info = count[k].querySelectorAll('input')
                    for(let i = 0; i< info.length; i++){
                        info[i].id = `addLine${total}`
                    }
                    total++;
                }else{
                    changeId(count[k].id);
                }
            }
            if(count.length == 0){
                $('#pre').attr('disabled','true')
            }
            dragEnd()
        })
        this.appendChild(div)
    }else if(dragItem.id == 'Fb'){
        let div = document.createElement('div');
            div.className = 'list-group-item'
            div.id = `facebook${number.length + 1}`
            div.style = 'width:auto;height:auto;'
        let span_txt = document.createTextNode("\u00D7");
        let h5 = document.createElement('h5')
            h5.appendChild(document.createTextNode(`ใส่ลิ้งค์ เพจ FaceBook`))
        let close = document.createElement('span');
            close.className = 'close btn-danger'
            close.classList.add('btn')
            close.appendChild(span_txt)
            div.appendChild(close)
            div.appendChild(h5);
        let input = document.createElement('input')
            input.classList.add('form-control')
            input.classList.add('mt-5')
            input.style = 'padding:10px;'
            input.type = "text" 
            input.setAttribute('placeholder','Url FaceBook')
            input.id = `pageFB${number.length + 1}`
            div.setAttribute('draggable','true')
            div.appendChild(input);
        close.addEventListener('click',()=>{
            this.removeChild(div)
            let count = col.querySelectorAll('.list-group-item')
            let countInput = col.querySelectorAll('input')
            let total =1
            for( let k = 0; k < count.length; k++ ){
                if(count[k].id.indexOf(`facebook`) != -1){
                    count[k]. id = `facebook${parseInt(k) + 1}`
                    let info = count[k].querySelectorAll('input')
                    for(let i = 0; i < info.length; i++){
                        info[i].id = `pageFB${total}`
                    }
                    total++;
                }else{
                    changeId(count[k].id);
                }
            }
            if(count.length == 0){
                $('#pre').attr('disabled','true')
            }
            dragEnd()
        })
        this.appendChild(div)
    }else if(dragItem.id =='SendMessage'){
        let div = document.createElement('div');
            div.className = 'list-group-item'
            div.id = `Send${number.length + 1}`
            div.style = 'width:auto;height:auto;'
        let span_txt = document.createTextNode("\u00D7");
        let h5 = document.createElement('h5')
            h5.appendChild(document.createTextNode(`ใส่ลิ้งค์ส่งข้อความเพจ FaceBook`))
        let close = document.createElement('span');
            close.className = 'close btn-danger'
            close.classList.add('btn')
            close.appendChild(span_txt)
            div.appendChild(close)
            div.appendChild(h5);
        let input = document.createElement('input')
            input.classList.add('form-control')
            input.classList.add('mt-4')
            input.style = 'padding:10px;'
            input.type = "text" 
            input.setAttribute('placeholder','Url Message FaceBook')
            input.id = `inbox${number.length + 1}`
            div.setAttribute('draggable','true')
            div.appendChild(input);
        close.addEventListener('click',()=>{
            this.removeChild(div)
            let count = col.querySelectorAll('.list-group-item')
            let countInput = col.querySelectorAll('input')
            let total = 1
            for( let k = 0; k < count.length; k++ ){
                if(count[k].id.indexOf(`Send`) != -1){
                    count[k]. id = `Send${parseInt(k) + 1}`
                    let info = count[k].querySelectorAll('input')
                    for(let i = 0; i< info.length; i++){
                        info[i].id = `inbox${total}`
                    }
                    total++;
                }else{
                    changeId(count[k].id);
                }
            }
            if(count.length == 0){
                $('#pre').attr('disabled','true')
            }
            dragEnd()
        })
        this.appendChild(div)

    }
    $('#pre').removeAttr('disabled')
    $('#pre').attr('data-bs-toggle','modal')
    $('#pre').attr('data-bs-target','#exampleModal')
    $('.dropify').dropify();
}
function changeId(Id){
    let col = document.querySelector('.column');
    let count = col.querySelectorAll('.list-group-item')
    let countInput = col.querySelectorAll('input')
    let total = 1;
    for(let k = 0; k < count.length; k++){
        //console.log(count[k].id);
        if(count[k].id.indexOf(`photoslide`) != -1){
            count[k]. id = `photoslide${parseInt(k) + 1}`
            let info = count[k].querySelectorAll('input')
            for(let i = 0; i< info.length; i++){
                info[i].id = `${k+1}imgSlide${i+1}`
            }
            total++;
        }
        else if(count[k].id.indexOf(`video`) != -1){
            count[k]. id = `video${parseInt(k) + 1}`
            let info = count[k].querySelectorAll('input')
            for(let i = 0; i< info.length; i++){
                info[i].id = `vdo${total}`
            }
            total++;
        }else if(count[k].id.indexOf(`onlyphoto`) != -1){
            count[k]. id = `onlyphoto${parseInt(k) + 1}`
            let info = count[k].querySelectorAll('input')
            for(let i = 0; i< info.length; i++){
                info[i].id = `img${total}`
            }
            total++;
        }else if(count[k].id.indexOf(`secondphoto`) != -1){
            count[k]. id = `secondphoto${parseInt(k) + 1}`
            let info = count[k].querySelectorAll('input')
            for(let i = 0; i< info.length; i++){
                info[i].id = `${k+1}imgsecond${i+1}`
            }
            total++;
        }else if(count[k].id.indexOf(`Urlphoto`) != -1){
            let info = count[k].querySelectorAll('input')
                count[k].id = `Urlphoto${k+1}`
                for(let i = 0 ; i < info.length ; i++){
                    if(info[i].id.indexOf(`imglink`) != -1){
                        info[i].id = `imglink${k+1}`
                    }else if(info[i].id.indexOf(`imgUrl`)!= -1){
                        info[i].id = `imgUrl${k+1}`
                    }
                }
                total++;
        }else if(count[k].id.indexOf(`photosecondUrl`) != -1){
            let info = count[k].querySelectorAll('input.dropify')
            let infourl = count[k].querySelectorAll('input[type="url"]')
            count[k].id = `photosecondUrl${k+1}`
            for(let i = 0 ; i < info.length ; i++){
                info[i].id = `${total}imgsecondlink${i+1}`
            }
            for(let i = 0 ; i < infourl.length ; i++){
                infourl[i].id = `${total}img2Url${i+1}`
            }
            total++;
        }else if(count[k].id.indexOf(`Message`) != -1){
            count[k]. id = `Message${parseInt(k) + 1}`
            let info = count[k].querySelectorAll('input')
            for(let i = 0; i< info.length; i++){
                info[i].id = `text${total}`
            }
            total++;
        }else if(count[k].id.indexOf(`Line`) != -1){
            count[k]. id = `Line${parseInt(k) + 1}`
            let info = count[k].querySelectorAll('input')
            for(let i = 0; i< info.length; i++){
                info[i].id = `addLine${total}`
            }
            total++;
        }else if(count[k].id.indexOf(`facebook`) != -1){
            count[k]. id = `facebook${parseInt(k) + 1}`
            let info = count[k].querySelectorAll('input')
            for(let i = 0; i< info.length; i++){
                info[i].id = `pageFB${total}`
            }
            total++;
        }else if(count[k].id.indexOf(`Send`) != -1){
            count[k].id = `Send${parseInt(k) + 1}`
            let info = count[k].querySelectorAll('input')
            for(let i = 0; i< info.length; i++){
                info[i].id = `inbox${total}`
            }
            total++;
        }
        if(count.length == 0){
            $('#pre').attr('disabled','true')
        }
    }
}


