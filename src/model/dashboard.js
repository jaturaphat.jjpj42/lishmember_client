const { lish_member } = require("../../config/Database")
const {format} = require('date-fns')

exports.Dashbord = class DashboardModel{

    async realtotal(shop_code){
        let data = []
        let total = await lish_member.query(`select *,to_char(lo.order_date,'yyyy-MM-dd') as order_date from lish_orders lo where order_status != 5 and shop_code = '${shop_code}'`)
        let total_month = total.rows.filter(e=>format(new Date(e.order_date),'MM') == format(new Date(),'MM') && e.order_status != 5 && e.delivery_status != 5 && e.delivery_status != 4).reduce((e,value)=>e + value.total,0)
        let total_today = total.rows.filter(e=>e.order_date == format(new Date(),'yyyy-MM-dd') && e.order_status != 5 && e.delivery_status != 5 && e.delivery_status != 4).reduce((e,value)=>e + value.total,0)
        let total_order = total.rows.filter(e=>format(new Date(e.order_date),'MM') == format(new Date(),'MM')).length
        let total_order_today = total.rows.filter(e=>format(new Date(e.order_date),'MM') == format(new Date(),'MM') && (e.order_status === 1 || e.order_status === 2 || e.order_status === 3) && e.delivery_status === 0).length
        let total_success = total.rows.filter(e=>format(new Date(e.order_date),'MM') == format(new Date(),'MM') && e.order_status === 4 && e.delivery_status === 3).length
        let total_delivery = total.rows.filter(e=>format(new Date(e.order_date),'MM') == format(new Date(),'MM') && e.order_status === 3 && e.delivery_status === 1).length
        let total_year = total.rows.filter(e=>format(new Date(e.order_date),'yyyy') == format(new Date(),'yyyy') && e.order_status != 5 && e.delivery_status != 5 && e.delivery_status != 4).reduce((e,value)=>e + value.total,0)
        data.push({month:total_month,today:total_today,order:total_order,order_today:total_order_today,success:total_success,delivery:total_delivery,total_year:total_year})
        return data[0]
    }

    async realChannel(shop_code){
        return await lish_member.query(`select sc.channel_name,sc.channel_id
        ,(select count(lo.social) from lish_orders lo where shop_code = '${shop_code}' and lo.social = sc.channel_id)
        ,(select count(lo.social) from lish_orders lo where shop_code = '${shop_code}') as sum
        from shop_channel sc 
        left join lish_channel lc on sc.channel_id = lc.id 
        where sc.status_channel = 1 
        and lc.on_delete = 'N' 
        and sc.shop_code = '${shop_code}'`)
    }
}