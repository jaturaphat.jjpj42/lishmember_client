const {oneoms,whoms,lish_member} = require('../../config/Database')
const SendNoti = require('../../config/LineNoti')
const axios = require('axios')
exports.Managequery = class Managequery{

    async queryOrder(shop_code){
        return await lish_member.query(`
        SELECT (select STRING_AGG(distinct CONCAT(lp.promotion_name ,'( x',CAST(lod.qty_promotion  as varchar),' )'), ', ') as product
        from lish_orders_details lod left join lish_promotion lp on lod.promotion_code = lp.promotion_code where lod.order_number = lo.order_number) as product,lo.id,lo.order_number, lo.first_name, lo.last_name, lo.tel, lo.tel2, lo.email, lo.address, lo.subdistrict, lo.district, lo.province, lo.zipcode, lo.price, discount, total, freight
        , lps.status_name as payment_name, los.status_name as order_name, lpt.payment_name, payment_status, order_status, to_char(order_date,'YYYY-MM-DD HH:MI:SS') as order_date
        , delivery_status, courier_tracking ,lpt.id as payment_type,lo.images,lc.image as channel_image
        FROM lish_orders lo
        left join lish_channel lc on lo.social = lc.id 
        LEFT JOIN lish_payment_status lps on  lo.payment_status = lps.id
        LEFT JOIN lish_orders_status los on lo.order_status = los.id
        LEFT JOIN lish_payment_type lpt on lo.payment = lpt.id Where lo.shop_code = '${shop_code}'`)
    }
    
    async UpdateOrder_status(status,id){
        return await lish_member.query(`
        Update lish_orders
        SET order_status = $1
        WHERE id = $2
        `,[status,id])
    }

    async UpdateOrder_payment(status,id){
        return await lish_member.query(`
        Update lish_orders
        SET order_status = $1, payment_status = 2
        WHERE id = $2
        `,[status,id])
    }

    async Bank(){
        return await lish_member.query(`
        select * from setting_bank
        `)
    }

    async payment(){
        return await lish_member.query(`
        select * from lish_payment_type
        where status = '1'
        `)
    }

    async delivery(){
        return await lish_member.query(`select * from lish_company_delivery where status = 1`)
    }

    async Check_stock(shop_code,order_number,status){
        let stock = await lish_member.query(`select * from shop_product sp where shop_code = '${shop_code}'`)
        let check = true
        for(let j = 0;j<order_number.length;j++){
            let detail = await lish_member.query(`select * from lish_orders_details where order_number = '${order_number[j].order_number}'`)
            for(let i = 0;i<detail.rows.length;i++){
                for(let data = 0;data<stock.rows.length;data++){
                    if(stock.rows[data].product_code == detail.rows[i].product_code){
                        if((stock.rows[data].quantity-stock.rows[data].qty_book)-detail.rows[i].quantity < 0){
                            let noti_msg = `สินค้า ${stock.rows[i].product_name} ไม่เพียงพอ`
                            let noti = await lish_member.query(`select noti_stock,line_token from setting_shop ss where ss.shop_code = '${shop_code}'`)
                            let line_token = noti.rows[0].line_token
                            let limit_stock = noti.rows[0].noti_stock
                            SendNoti(noti_msg,line_token,(err,reslut)=>{})
                            check = false
                            return(check)
                        }else{
                            stock.rows[data].qty_book += detail.rows[i].quantity
                            // console.log(stock.rows[data].qty_book,detail.rows[i].quantity,detail.rows[i].product_code,order_number[j].order_number)
                        }
                    }
                }
            }
        }
        if(check){
            let noti = await lish_member.query(`select noti_stock,line_token from setting_shop ss where ss.shop_code = '${shop_code}'`)
            let line_token = noti.rows[0].line_token
            let limit_stock = noti.rows[0].noti_stock
            for(let i = 0;i<stock.rows.length;i++){
                await lish_member.query(`update shop_product set qty_book=${stock.rows[i].qty_book} where shop_code='${shop_code}' and product_code = '${stock.rows[i].product_code}'`)
                if(stock.rows[i].quantity-stock.rows[i].qty_book <= limit_stock && stock.rows[i].status == 1){
                    let noti_msg = `สินค้า ${stock.rows[i].product_name} ถึงจำนวนที่กำหนดกรุณาเปิดบิลเพื่อสั่งสินค้า`
                    if(line_token != '' || line_token != null){
                        SendNoti(noti_msg,line_token,(err,reslut)=>{
                            if(err){
                                console.log(err)
                                return(check)
                            }else{
                                return(check)
                            }
                        })
                    }
                }
            }
            return(check)
        }
    }

    
    async deliver_shop(shop_code){
        return await lish_member.query(`select *,sd.id as id from setting_shop_transport sst left join setting_delivery sd on sst.transport_id = sd.delivery_code where shop_code = '${shop_code}'`)
    }

    async cancel_shipjung(order_number){
        let order = await lish_member.query(`select * from lish_orders where order_number = '${order_number}'`)
        let tracking_code = order.rows[0].courier_booking
        // let date = format(new Date(),'yyyy-MM-dd HH:mm:ss')
        let brand_api  = await oneoms.query(`SELECT * FROM oms_brand WHERE brand_code  = 'LISH'`)
        let check = true
        const payload = {};
        const axiosConfig = {
            headers: {
            "Content-Type": "application/json",
            Authorization: `Token ${brand_api.rows[0].api_key}`,
            },
        };
        axios.post(process.env.url + `v1/orders/cancel/${tracking_code}/`,payload,axiosConfig).then((result)=>{
            if(result.status == 200){
                let update_order = lish_member.query(`update lish_orders_logs set status=5,order_status=5 where order_number = '${order_number}'`)
            }else{
                check = false
            }
        }).catch((err)=>{console.log(err);check = false})
        if(check){
            return true
        }else{
            return false
        }
    }
}