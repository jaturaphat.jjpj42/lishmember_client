const {oneoms,whoms,lish_member} = require('../../config/Database')

exports.apimodel = class apimodel{

    async bank(){
        return await lish_member.query(`SELECT * FROM setting_bank`)
    }

    async transport(){
        return await lish_member.query(`SELECT * FROM setting_delivery`)
    }

    async promotion_detail(){
        return await lish_member.query(`SELECT * FROM lish_promotion_detail where status = 1`)
    }

}