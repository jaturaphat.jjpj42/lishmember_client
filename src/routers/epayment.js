var express = require('express');
var app = express.Router();
const epayment = require('../controller/epayment')

app.get('/',epayment.paymentview);
app.post('/thank',epayment.thankyouview);

module.exports = app;