const express = require('express');
const app = express.Router();
const CreativeStore = require('../controller/creativestore.js')


app.get('/',(req,res)=>{new CreativeStore.creativestore().views(req,res)})
app.post('/savedata',(req,res)=>{new CreativeStore.creativestore().savedata(req,res)})
app.get('/getdata',(req,res)=>{new CreativeStore.creativestore().getdata(req,res)})
module.exports = app;