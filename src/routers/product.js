const express = require('express')
const product = express.Router()
const productctl = require('../controller/product')


product.get('/',async (req,res)=>{new productctl.product().view(req,res)})
product.get('/linklish:id',async (req,res)=>{new productctl.add_order().views(req,res)})
product.get('/linklish/tranfer',async (req,res)=>{new productctl.add_order().view_upload(req,res)})
product.get('/linklish/thank',async (req,res)=>{new productctl.add_order().view_Thank(req,res)})
product.get('/epayment',async (req,res)=>{new productctl.add_order().epayment(req,res)})
product.post('/insert_order',async (req,res)=>{new productctl.add_order().insert_order(req,res)})
product.post('/linklish/confirm',async (req,res)=>{new productctl.add_order().view_confirm(req,res)})
product.get('/promotion:id',async (req,res)=>{new productctl.promotion().views(req,res)})
product.get('/add_promotion',async (req,res)=>{new productctl.promotion().views_add(req,res)})
product.get('/update_promotion/:id',async (req,res)=>{new productctl.promotion().views_update(req,res)})
product.post('/insert_promotion',async (req,res)=>{new productctl.promotion().insert_promotion(req,res)})
product.post('/update_promotion',async (req,res)=>{new productctl.promotion().update_promotion(req,res)})
product.post('/delete_promotion',async (req,res)=>{new productctl.promotion().delete_promotion(req,res)})
product.post('/swtich_status',async (req,res)=>{new productctl.promotion().swtich_status(req,res)})

product.get('/test/:id',async (req,res)=>{new productctl.promotion().views(req,res)})

module.exports = product
