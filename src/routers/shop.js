const express = require('express');
const app = express.Router();
const ShopCtrl = require('../controller/shop.js');

app.get('/',(req,res)=>{new ShopCtrl.shop().views(req,res)})
app.get('/getData',(req,res)=>{new ShopCtrl.shop().getData(req,res)})
app.get('/productdetail/',(req,res)=>{new ShopCtrl.shop().productdetail(req,res)})
app.get('/confirm_order',(req,res)=>{new ShopCtrl.shop().confirm_order(req,res)})
app.get('/ordersuccess:id',(req,res)=>{new ShopCtrl.shop().order_success(req,res)})
app.post('/epayment',(req,res)=>{new ShopCtrl.shop().epayment(req,res)})
app.post('/keep_info',(req,res)=>{new ShopCtrl.shop().keep_info(req,res)})
app.post('/buyorder:id',(req,res)=>{new ShopCtrl.shop().buyorder(req,res)})
app.post('/return',(req,res)=>{new ShopCtrl.shop().returned(req,res)})
module.exports = app;