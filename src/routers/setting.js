const express = require('express')
const setting = express.Router()
const settingctl = require('../controller/setting')

setting.get('/',(req,res)=>{new settingctl.setting().views(req,res)})
setting.get('/update:shop',(req,res)=>{new settingctl.setting().views_update(req,res)})
setting.post('/add_shop',(req,res)=>{new settingctl.setting().add_shop(req,res)})
setting.post('/update_shop',(req,res)=>{new settingctl.setting().update_shop(req,res)})
setting.get('/ManagerChannel',(req,res)=>{new settingctl.setting().viewsChannel(req,res)})
setting.post('/addChannel',(req,res)=>{new settingctl.setting().Addchannel(req,res)})
setting.post('/updatechannel',(req,res)=>{new settingctl.setting().Updatechannel(req,res)})
setting.post('/deletechannel',(req,res)=>{new settingctl.setting().Deletechannel(req,res)})
setting.post('/changechannel',(req,res)=>{new settingctl.setting().Changechannel(req,res)})
module.exports = setting