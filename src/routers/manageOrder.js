const express = require('express');
const app = express.Router();
const ManageCtrl = require('../controller/manageOrder');



app.get('/',(req,res)=>{new ManageCtrl.ManageOrder().views(req,res)})
app.get('/addOrder',(req,res)=>{new ManageCtrl.ManageOrder().viewsAddorder(req,res)})
app.get('/updateOrder:order_code',(req,res)=>{new ManageCtrl.ManageOrder().viewsUpdateorder(req,res)})
app.get('/getshop_item:shop_code',(req,res)=>{new ManageCtrl.ManageOrder().getshop_item(req,res)})
app.get('/get_order:order_code',(req,res)=>{new ManageCtrl.ManageOrder().get_order(req,res)})
app.post('/insert_order',(req,res)=>{new ManageCtrl.ManageOrder().insert_order(req,res)})
app.post('/update_order',(req,res)=>{new ManageCtrl.ManageOrder().update_order(req,res)})
app.post('/UpdateOrder',(req,res)=>{new ManageCtrl.ManageOrder().api_UpdateOrder(req,res)})
app.post('/UpdateDelivery',(req,res)=>{new ManageCtrl.ManageOrder().api_UpdateDelivery(req,res)})
app.get('/export:data',(req,res)=>{new ManageCtrl.ManageOrder().pdf(req,res)})

module.exports = app;