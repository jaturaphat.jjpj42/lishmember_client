const express = require('express')
const api = express.Router()
const apictl = require('../controller/api')

api.get('/bank',(req,res)=>{new apictl.api().bank(req,res)})
api.get('/transport',(req,res)=>{new apictl.api().transport(req,res)})
api.post('/check_name',(req,res)=>{new apictl.api().check_name(req,res)})
api.post('/insert_order',async (req,res)=>{new apictl.api().insert_order(req,res)})
api.get('/linkLish/Thank',async (req,res)=>{new apictl.api().view_Thank(req,res)})
api.get('/list_promotion:shop_code',async (req,res)=>{new apictl.api().promotion_list(req,res)})
api.get('/list_promotion_detail',async (req,res)=>{new apictl.api().promotion_list_detail(req,res)})
api.post('/insert_order_client',async (req,res)=>{new apictl.api_shop().insert_db(req,res)})


api.get('/test_pdf',async (req,res)=>{new apictl.api().pdf(req,res)})

module.exports = api