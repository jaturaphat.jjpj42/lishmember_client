const express = require('express')
const wh = express.Router()
const whctl = require('./../controller/wh')

wh.get('/shop/:shop_code',(req,res)=>{ new whctl.whctl().view(req,res)})
wh.get('/event/',(req,res)=>{new whctl.whctl().view_event(req,res)})
wh.post('/add',(req,res)=>{new whctl.whctl().add(req,res)})
wh.post('/delete_wh',(req,res)=>{new whctl.whctl().delete_wh(req,res)})
wh.post('/change',(req,res)=>{new whctl.whctl().change(req,res)})
wh.post('/noti_stock',(req,res)=>{new whctl.whctl().noti(req,res)})

module.exports = wh