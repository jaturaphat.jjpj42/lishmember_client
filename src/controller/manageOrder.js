const { lish_member } = require('../../config/Database')
const ManageModel = require('../model/manageOrder')
const {format} = require('date-fns')
const multer =require("multer")
const crypto = require("crypto");
const PDFDocument = require("pdfmake-thai");
const fs = require("fs");
var fonts = {
    THSarabunNew: {
        normal: './src/public/font_pdf/Sarabun/Sarabun-Regular.ttf',
        bold: './src/public/font_pdf/Sarabun/Sarabun-Medium.ttf',
        italics: './src/public/font_pdf/Sarabun/Sarabun-Italic.ttf',
        bolditalics: './src/public/font_pdf/Sarabun/Sarabun-MediumItalic.ttf'
    },
    Roboto: {
        normal: './src/public/font_pdf/Roboto/Roboto-Regular.ttf',
        bold: './src/public/font_pdf/Roboto/Roboto-Medium.ttf',
        italics: './src/public/font_pdf/Roboto/Roboto-Italic.ttf',
        bolditalics: './src/public/font_pdf/Roboto/Roboto-MediumItalic.ttf'
    }
};

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

var storage1 = multer.diskStorage({
    destination: function (req, file, callback) {
    var dir = "./src/public/image/order";
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    callback(null, dir);
    },
    filename: function (req, file, callback) {
    let randomname = makeid(5)
    let name = file.originalname
    let name_data = name.substring(0,3)
    let datetime = format(new Date,"yyyyMMddHHmmss")
    callback(null, `${name_data}${randomname}-${datetime}.jpg`);
    },
});

var uploadpicturefields = multer({ storage: storage1 }).fields([{name:"file1"},{name:"file2"},{name:"file3"},{name:"file4"}]);
var uploadpictureArray= multer({ storage: storage1 }).array("files");

exports.ManageOrder = class ManageOrder{

    async api_UpdateOrder (req,res){
        let {id,status} = req.body
        let ManageQuery = new ManageModel.Managequery()
        
        switch(status){
            case 2:
                await ManageQuery.UpdateOrder_payment(status,id).then(()=>{
                    res.sendStatus(200)
                }).catch((error)=>{
                    console.log(error)
                    res.sendStatus(400)
                })
                break;
            case 3:
                let check = await ManageQuery.Check_stock(req.user.shop_code,id,status)
                if(check){
                    for(let i = 0;i<id.length;i++){
                        let id_cal = await lish_member.query(`select id from lish_orders where order_number = '${id[i].order_number}'`)
                        await ManageQuery.UpdateOrder_status(status,id_cal.rows[0].id)
                    }
                    res.send({Message:'Success'})
                }else{
                    res.send({Message:'สินค้าไม่เพียงพอ'})
                }
                break;
            case 5:
                for(let i = 0;i<id.length;i++){
                    let id_cal = await lish_member.query(`select * from lish_orders where order_number = '${id[i].order_number}'`)
                    let order_detail = await lish_member.query(`select * from lish_orders_details where order_number = '${id[i].order_number}'`)
                    let shop_product = await lish_member.query(`select * from shop_product sp where sp.shop_code = '${req.user.shop_code}'`)
                    if(req.user.type_sent == 0){
                        if(id_cal.rows[0].order_status == 3){
                            order_detail.rows.forEach(async (data)=>{
                                let qty = shop_product.rows.filter((e)=>e.product_code === data.product_code)
                                let update = await lish_member.query(`update shop_product set qty_book=${qty[0].qty_book - data.quantity} where shop_code = '${req.user.shop_code}'`)
                            })
                        }
                        await ManageQuery.UpdateOrder_status(status,id_cal.rows[0].id)
                        res.send({Message:'Success'})
                    }else{
                        if(id_cal.rows[0].order_status == 3){
                            order_detail.rows.forEach(async (data)=>{
                                let qty = shop_product.rows.filter((e)=>e.product_code === data.product_code)
                                let update = await lish_member.query(`update shop_product set qty_book=${qty[0].qty_book - data.quantity} where shop_code = '${req.user.shop_code}'`)
                            })
                        }else if(id_cal.rows[0].order_status == 6){
                            let check = await ManageQuery.cancel_shipjung(id[i].order_number)
                            if(check){
                                order_detail.rows.forEach(async (data)=>{
                                    let qty = shop_product.rows.filter((e)=>e.product_code === data.product_code)
                                    let update = await lish_member.query(`update shop_product set qty_book=${qty[0].qty_book - data.quantity} where shop_code = '${req.user.shop_code}'`)
                                })
                                await ManageQuery.UpdateOrder_status(status,id_cal.rows[0].id)
                                res.send({Message:'Success'})
                            }else{
                                res.send({Message:'Error'})
                            }
                        }
                    }
                    
                }
                break;
        }
    }

    async api_UpdateDelivery (req,res){
        let {tracking_code,delivery,id} = req.body
        let delivery_raw = await lish_member.query(`select * from setting_delivery where status = 1 and id = ${delivery}`)
        // if(req.user.type_sent == 0){

        // }else{

        // }
        let command = `Update lish_orders set delivery='${delivery}',courier_code='${delivery_raw.rows[0].code}',courier_tracking='${tracking_code}' `
        if(req.user.type_sent == 0){
            command += ` ,delivery_status=2,order_status=4 where id = ${id}`
        }else{
            command += ` ,delivery_status=1 where id = ${id}`
        }
        await lish_member.query(command,async (err,result)=>{ 
            if(err){
                console.log(err)
                res.send({Message:'Error'})
            }else{
                res.send({Message:'Success'})
            }
        })
    }

    async views(req,res) {
        let ManageQuery = new ManageModel.Managequery()
        let data = await ManageQuery.queryOrder(req.user.shop_code)
        data.rows.forEach((e)=>{
            if(e.images != ''){
                let imge = JSON.parse(e.images)
                if(imge.length != 0 ){
                    console.log(imge)
                    e.images = imge[0].img
                }
            }
            console.log(e.images)
        })
        let type_account = req.user.type_sent
        let delivery_shop = await ManageQuery.deliver_shop(req.user.shop_code)
        let delivery = await ManageQuery.delivery()
        res.render('./order/ManageOrder',{data:data.rows,type_account:type_account,delivery:delivery.rows,delivery_shop:delivery_shop.rows})
    }

    async viewsAddorder(req,res) {
        let ManageQuery = new ManageModel.Managequery()
        let bank = await ManageQuery.Bank()
        let payment = await ManageQuery.payment()
        const shop_code = req.user.shop_code
        let data = await lish_member.query(`select sp.promotion_name,sp.promotion_code,price from shop_promotion sp left join lish_promotion lp on
        sp.promotion_code = lp.promotion_code where sp.shop_code = '${shop_code}' and sp.status = 1`)
        let promotion = await lish_member.query(`select * from shop_promotion sp left join lish_promotion lp on sp.promotion_code = lp.promotion_code where sp.status = 1 and sp.shop_code = '${req.user.shop_code}'`)
        let Channel = await lish_member.query(`select * from shop_channel where (shop_code = 'Admin' or shop_code = '${req.user.shop_code}') and on_delete = 'N' and status_channel = 1`)
        res.render('./order/AddOrder',{bank:bank.rows,payment:payment.rows,promotion:promotion.rows,Channel:Channel.rows,order_code:`0`,prmotion_detail:data.rows})
    }

    async viewsUpdateorder(req,res) {
        let order_code = req.params.order_code
        let ManageQuery = new ManageModel.Managequery()
        let bank = await ManageQuery.Bank()
        let payment = await ManageQuery.payment()
        const shop_code = req.user.shop_code
        let data = await lish_member.query(`select sp.promotion_name,sp.promotion_code,price from shop_promotion sp left join lish_promotion lp on
        sp.promotion_code = lp.promotion_code where sp.shop_code = '${shop_code}' and sp.status = 1`)
        console.log(data.rows)
        let promotion = await lish_member.query(`select * from shop_promotion sp left join lish_promotion lp on sp.promotion_code = lp.promotion_code where sp.status = 1 and sp.shop_code = '${req.user.shop_code}'`)
        let Channel = await lish_member.query(`select * from lish_channel where shop_code = 'Admin' or shop_code = '${req.user.shop_code}'`)
        res.render('./order/AddOrder',{bank:bank.rows,payment:payment.rows,promotion:promotion.rows,Channel:Channel.rows,order_code:order_code,prmotion_detail:data.rows})
    }

    async get_order(req,res){
        let order_code = req.params.order_code
        let order = await lish_member.query(`select * from lish_orders where order_number = '${order_code}'`)
        let order_detail = await lish_member.query(`select distinct lod.promotion_code,qty_promotion from lish_orders_details lod where order_number = '${order_code}'`)
        res.send({order:order.rows,order_details:order_detail.rows})
    }

    async insert_order(req,res){
        uploadpicturefields(req,res,async function (err) {
            if(err){
                console.log(err)
            }else{
                let data = req.body
                let check = true
                await lish_member.query(`select * from shop_product sp where shop_code = '${data.shop_code}'`,async (err,result)=>{
                    if(err){
                        console.log(err)
                        res.send({Message:'Error'})
                    }else{
                        let count = parseInt(data.cart_len)
                        if(count == 1){
                            let item_detail = await lish_member.query(`select * from lish_promotion_detail where promotion_code = '${data.promotion_code}'`)
                            result.rows.forEach((shop_item)=>{
                                item_detail.rows.forEach((item)=>{
                                    if(shop_item.product_code == item.product_code){
                                        // console.log(shop_item.quantity,item.quantity)
                                        if(shop_item.quantity >= parseInt(item.quantity)*parseInt(data.amount)){
                                            shop_item.quantity = shop_item.quantity-(parseInt(item.quantity)*parseInt(data.amount))
                                        }else{
                                            check = false
                                        }
                                    }
                                })
                            })
                        }else{
                            for(let i = 0;i < data.promotion_code.length;i++){
                                let item_detail = await lish_member.query(`select * from lish_promotion_detail where promotion_code = '${data.promotion_code[i]}'`)
                                for(let shop = 0;shop<result.rows.length;shop++){
                                    let shop_item = result.rows[shop]
                                    for(let itemlen = 0;itemlen<item_detail.rows.length;itemlen++){
                                        let item = item_detail.rows[itemlen]
                                        if(shop_item.product_code == item.product_code){
                                            if(shop_item.quantity >= parseInt(item.quantity)*parseInt(data.amount[i])){
                                                shop_item.quantity = shop_item.quantity-(parseInt(item.quantity)*parseInt(data.amount[i]))
                                            }else{
                                                check = false
                                            }
                                            // console.log(shop_item.product_code,parseInt(item.quantity)*parseInt(data.amount[i]),shop_item.quantity)
                                        }
                                    }
                                }
                            }
                        }
                        if(check){
                            let check_count = await lish_member.query(`SELECT order_number from lish_orders where DATE(order_date) = DATE(now()) and shop_code = '${req.user.shop_code}' ORDER by id desc limit 1`)
                            let yearsnow = format(new Date(),'yyyyMMdd')
                            let shop = `${parseInt('0000')+parseInt(data.shop_id)}`
                            let tel = data.tel.slice(-3)
                            let code_text = `0`
                            if(check_count.rows.length != 0){
                                code_text = (check_count.rows[0].order_number).substring(14, 18)
                                code_text = parseInt(code_text)+1
                            }else{
                                code_text = `0001`;
                            }
                            let code = `L${String(parseInt(shop)).padStart(4, '0')}-${yearsnow}${String(parseInt(code_text)).padStart(4, '0')}${tel}`
                            let img = []
                            let payment_status = 1
                            let order_status = 1
                            if(req.files.file1 != undefined){
                                img.push({img:`image/order/${req.files.file1[0].filename}`})
                                // fs.unlinkSync(`./src/public/image/order/${req.files[0].filename}`)
                            }
                            let img_json = JSON.stringify(img)
                            console.log(img_json)
                            if(parseInt(data.payment) == 1 || parseInt(data.payment) == 6 || parseInt(data.payment) == 5){order_status = 2;data.bank=0}
                            await lish_member.query(`INSERT INTO lish_orders 
                            (order_number, first_name, last_name, tel, tel2, email, address
                                , subdistrict, district, province, zipcode, payment, payment_status
                                , delivery, delivery_status, courier_code, courier_tracking, courier_booking
                                , bank, images, marketplace_tracking, social, reason, shop_code
                                , order_date,price, discount, freight, total, order_status,remak,channel_remak) 
                            VALUES('${code}', '${data.first_name}', '${data.last_name}', '${data.tel}', '${data.tel2}', '${data.email}',
                            '${data.address}', '${data.subdistrict}', '${data.district}', '${data.province}', '${data.zipcode}'
                            , ${data.payment}, ${payment_status}, 0, 0, null, null, null, 0, '${img_json}'
                            , '${data.MKP_code}', '${data.Channel}', '', '${data.shop_code}', now(), ${data.total}, 0, ${data.delivery}, ${parseInt(data.total)+parseInt(data.delivery)}, ${order_status},'${data.remak}','${data.channel_remak}');`
                            ,async (err,result)=>{
                                if(err){
                                    console.log(err)
                                    res.send({Message:'Error'})
                                }else{
                                    let count = parseInt(data.cart_len)
                                    let check = true
                                    if(count == 1){
                                        let detail_item = await lish_member.query(`select * from lish_promotion_detail lpd left join lish_product_user lp on lpd.product_code = lp.product_code_user where promotion_code = '${data.promotion_code}'`)
                                        detail_item.rows.forEach(async (element)=>{
                                            await lish_member.query(`INSERT INTO lish_orders_details 
                                            (order_number, shop_code, product_code, product_name, price, quantity, total, promotion_code, qty_promotion) 
                                            VALUES('${code}', '${data.shop_code}', '${element.product_code}', '${element.product_name}', ${element.price}, ${parseInt(element.quantity)*parseInt(data.amount)}, ${parseInt(element.price)*parseInt(data.amount)}, '${element.promotion_code}', ${data.amount});`,async (err,response)=>{if(err){check=false;res.send({Message:'Error'})}})
                                        })
                                    }else{
                                        for(let i = 0;i<count;i++){
                                            let detail_item = await lish_member.query(`select * from lish_promotion_detail lpd left join lish_product_user lp on lpd.product_code = lp.product_code_user where promotion_code = '${data.promotion_code[i]}'`)
                                            detail_item.rows.forEach(async (element)=>{
                                                await lish_member.query(`INSERT INTO lish_orders_details 
                                                (order_number, shop_code, product_code, product_name, price, quantity, total, promotion_code, qty_promotion) 
                                                VALUES('${code}', '${data.shop_code}', '${element.product_code}', '${element.product_name}', ${element.price}, ${parseInt(element.quantity)*parseInt(data.amount[i])}, ${parseInt(element.price)*parseInt(data.amount[i])}, '${data.promotion_code[i]}', ${data.amount[i]});`,async (err,response)=>{if(err){check=false;res.send({Message:'Error'})}})
                                            })
                                        }
                                    }
                                    if(check){
                                        res.send({Message:'Success'})
                                    }
                                }
                            })
                        }else{
                            res.send({Message:'สินค้าไม่เพียงพอ'})
                        }
                    }
                })
            }
        })
    }

    async update_order(req,res){
        uploadpicturefields(req,res,async function (err) {
            if(err){
                console.log(err)
            }else{
                let data = req.body
                let check = true
                let msg = ``
                await lish_member.query(`select * from shop_product sp where shop_code = '${data.shop_code}'`,async (err,result)=>{
                    if(err){
                        console.log(err)
                        res.send({Message:'Error'})
                    }else{
                        let count = parseInt(data.cart_len)
                        if(count == 1){
                            let item_detail = await lish_member.query(`select * from lish_promotion_detail where promotion_code = '${data.promotion_code}'`)
                            result.rows.forEach((shop_item)=>{
                                item_detail.rows.forEach((item)=>{
                                    if(shop_item.product_code == item.product_code){
                                        if(shop_item.quantity >= parseInt(item.quantity)*parseInt(data.amount)){
                                            shop_item.quantity = shop_item.quantity-(parseInt(item.quantity)*parseInt(data.amount))
                                        }else{
                                            check = false
                                            msg = 'สินค้าไม่เพียงพอ'
                                        }
                                    }
                                })
                            })
                        }else{
                            for(let i = 0;i < data.promotion_code.length;i++){
                                let item_detail = await lish_member.query(`select * from lish_promotion_detail where promotion_code = '${data.promotion_code[i]}'`)
                                for(let shop = 0;shop<result.rows.length;shop++){
                                    let shop_item = result.rows[shop]
                                    for(let itemlen = 0;itemlen<item_detail.rows.length;itemlen++){
                                        let item = item_detail.rows[itemlen]
                                        if(shop_item.product_code == item.product_code){
                                            if(shop_item.quantity >= parseInt(item.quantity)*parseInt(data.amount[i])){
                                                shop_item.quantity = shop_item.quantity-(parseInt(item.quantity)*parseInt(data.amount[i]))
                                            }else{
                                                check = false
                                                msg = 'สินค้าไม่เพียงพอ'
                                            }
                                            // console.log(shop_item.product_code,parseInt(item.quantity)*parseInt(data.amount[i]),shop_item.quantity)
                                        }
                                    }
                                }
                            }
                        }
                        let code = data.order_code
                        let order_status = 1
                        let old_order = await lish_member.query(`select * from lish_orders where order_number = '${code}'`)
                        let img = []
                        let payment_status = 1
                        let img_old 
                        if(old_order.rows[0].images != ''){
                            img_old = JSON.parse(old_order.rows[0].images)
                        }
                        if(parseInt(data.payment) == 2){
                            if(req.files.file1 != undefined){
                                if(img_old.length != 0){
                                    fs.unlinkSync(`./src/public/${img_old[0].img}`)
                                }
                                img.push({img:`image/order/${req.files.file1[0].filename}`})
                                // console.log(req.files.file1[0])
                            }else{
                                if(JSON.parse(old_order.rows[0].images).length != 0){
                                    img.push(img_old[0])
                                }else{
                                    check = false
                                    msg = 'กรุณาใส่รูปสลิป'
                                }
                            }
                        }
                        let img_json = JSON.stringify(img)
                        if(check){
                            if(parseInt(data.payment) == 1 || parseInt(data.payment) == 5 || parseInt(data.payment) == 6){order_status = 2;}
                            await lish_member.query(`Update lish_orders set 
                            first_name = '${data.first_name}', last_name = '${data.last_name}', tel = '${data.tel}', tel2 = '${data.tel2}'
                            , email = '${data.email}', address = '${data.address}', subdistrict = '${data.subdistrict}'
                            , district = '${data.district}', province = '${data.province}', zipcode = '${data.zipcode}'
                            , payment = ${data.payment}, payment_status = ${payment_status}, delivery = 0
                            , delivery_status = 0, courier_code = null, courier_tracking = null, courier_booking = null
                            , bank = ${data.bank} , images = '${img_json}', marketplace_tracking = '${data.MKP_code}'
                            , social = '${data.Channel}', shop_code = '${data.shop_code}'
                            , order_date = now(),price = ${data.total}, discount = 0, freight = ${data.delivery}, total = ${parseInt(data.total)+parseInt(data.delivery)}
                            , order_status = ${order_status},remak = '${data.remak}',channel_remak='${data.channel_remak}' where order_number = '${code}'`
                            ,async (err,result)=>{
                                if(err){
                                    console.log(err)
                                    res.send({Message:'Error'})
                                }else{
                                    let count = parseInt(data.cart_len)
                                    let check = true
                                    let delete_old = await lish_member.query(`delete from lish_orders_details where order_number = '${code}'`)
                                    if(count == 1){
                                        let detail_item = await lish_member.query(`select * from lish_promotion_detail lpd left join lish_product_user lp on lpd.product_code = lp.product_code_user where promotion_code = '${data.promotion_code}'`)
                                        detail_item.rows.forEach(async (element)=>{
                                            await lish_member.query(`INSERT INTO lish_orders_details 
                                            (order_number, shop_code, product_code, product_name, price, quantity, total, promotion_code, qty_promotion) 
                                            VALUES('${code}', '${data.shop_code}', '${element.product_code}', '${element.product_name}', ${element.price}, ${parseInt(element.quantity)*parseInt(data.amount)}, ${parseInt(element.price)*parseInt(data.amount)}, '${element.promotion_code}', ${data.amount});`,async (err,response)=>{if(err){check=false;res.send({Message:'Error'})}})
                                        })
                                    }else{
                                        for(let i = 0;i<count;i++){
                                            let detail_item = await lish_member.query(`select * from lish_promotion_detail lpd left join lish_product_user lp on lpd.product_code = lp.product_code_user where promotion_code = '${data.promotion_code[i]}'`)
                                            detail_item.rows.forEach(async (element)=>{
                                                await lish_member.query(`INSERT INTO lish_orders_details 
                                                (order_number, shop_code, product_code, product_name, price, quantity, total, promotion_code, qty_promotion) 
                                                VALUES('${code}', '${data.shop_code}', '${element.product_code}', '${element.product_name}', ${element.price}, ${parseInt(element.quantity)*parseInt(data.amount[i])}, ${parseInt(element.price)*parseInt(data.amount[i])}, '${data.promotion_code[i]}', ${data.amount[i]});`,async (err,response)=>{if(err){check=false;res.send({Message:'Error'})}})
                                            })
                                        }
                                    }
                                    if(check){
                                        res.send({Message:'Success'})
                                    }
                                }
                            })
                        }else{
                            res.send({Message:msg})
                        }
                    }
                })
            }
        })
    }

    async getshop_item(req,res){
        let shop_code = req.params.shop_code
        let item = await lish_member.query(`select * from shop_product where shop_code = '${shop_code}'`)
        res.send(item.rows)
    }

    async pdf(req,res){
        var printer = new PDFDocument(fonts);
        var fs = require('fs');
        var docDefinition = {
            info: {
                title: 'Lish_'+format(new Date(),'yyyMMdd'),
                author: 'Lish_'+format(new Date(),'yyyMMdd'),
                subject: 'Lish_'+format(new Date(),'yyyMMdd'),
                keywords: 'Lish_'+format(new Date(),'yyyMMdd'),
            },
            content: [],
            pageSize: {height: 425.25, width: 283.5},
            pageMargins: [ 5, 5, 5, 5],
            styles: {
                header: {
                fontSize: 12,
                }
            },
            defaultStyle: {
            font: 'THSarabunNew'
            }
        };
        let list_data = req.params.data.split(',')
        let order_number = []
        list_data.forEach((element)=>{
            if(element != ''){
                order_number.push(element)
            }
        })
        let address = await lish_member.query(`select concat(ss.shop_name,' ',ss.phone) as name,concat(ss.address,' ',ss.subdistrict,' ',ss.district,' ',ss.province,' ',ss.zipcode) as address from setting_shop ss left join member_lish ml on ss.shop_code = ml.shop_code where ss.shop_code =  '${req.user.shop_code}'`)

        for(let main = 0;main < order_number.length ;main++){
            let data = await lish_member.query(`select *,concat(address,' ',subdistrict,' ',district,' ',province,' ',zipcode) as address from lish_orders where shop_code = '${req.user.shop_code}' and order_number = '${order_number[main]}'`)
            let order_details = await lish_member.query(`select distinct lpd.promotion_code, lp.promotion_name,lpd.qty_promotion from lish_orders_details lpd left join lish_promotion lp on lpd.promotion_code = lp.promotion_code where lpd.order_number = '${order_number[main]}'`)
            let address_customer = `${data.rows[0].first_name} ${data.rows[0].last_name} `
            let payment = `ไม่ต้องเก็บเงิน`
            if(data.rows[0].payment == 1){
                payment = `เก็บเงินปลายทาง`
                data.rows[0].total += ' บาท'
            }else{
                data.rows[0].total = `-`
            }
            docDefinition.content.push({image:'./src/public/assets/img/logo-black-01.png',width:40,alignment:'center',margin:[0,10,0,0]})
            docDefinition.content.push({
                style: 'tableExample',margin: [ 0, 0, 0, 5 ],
                table: {
                    widths: ['30%', '10%', '25%', '35%'],
                    heights: [20,20,10,10],
                    headerRows: 1,
                    body: [
                        [
                            {
                                text: 'ผู้ส่ง : '+address.rows[0].name+'\n'+address.rows[0].address,
                                colSpan: 3,
                                rowSpan:2,
                                bold: true,
                                fontSize: 9
                            },{},{},
                            {
                                text: 'Phone',
                                alignment: 'center',
                                rowSpan:[1,2],
                                fontSize: 10,
                                bold: true,
                            },
                        ],
                        [
                            {},{},{},{
                                text: data.rows[0].tel,
                                alignment: 'center',
                                colSpan:1,
                                rowSpan:1,
                                fontSize: 12,
                                bold: true,
                                margin:[10,0,0,0]
                            }
                        ],
                        [
                            {
                                text: 'ผู้รับ : '+address_customer+''+data.rows[0].tel+'\n'+data.rows[0].address,
                                colSpan: 3,
                                rowSpan:2,
                                bold: true,
                                fontSize: 9
                            },{},{},{
                                text: payment,
                                alignment: 'center',
                                colSpan:1,
                                rowSpan:1,
                                fontSize: 10,
                            }
                        ],
                        [
                            {},{},{},{text: data.rows[0].total ,alignment:'center'}
                        ],
                        [
                            {
                                text: data.rows[0].order_number,
                                alignment:'center',
                                colSpan:3,
                                rowSpan:2,
                                fontSize: 14,
                                bold: true,
                                margin: [0, 8, 0, 0]
                            }, 
                            { 
                            },
                            {
                            },
                            {
                                text: 'ZIPCODE',
                                alignment:'center',
                                colSpan: 1,
                                rowSpan:[2,3],
                                fontSize: 8,
                                bold: true,
                                margin:[0,0,0,0]
                            }
                        ],
                        [
                            {
                            }, 
                            { 
                            },
                            {
                            },
                            {   
                                text: data.rows[0].zipcode,
                                alignment:'center',
                                colSpan: 1,
                                rowSpan:1,
                                fontSize: 18,
                                bold: true,
                                margin:[0,-2,0,0]
                            },
                        ],
                    ]
                }
            })
            docDefinition.content.push({image:'./src/public/assets/img/cut.jpg',width:280.5})
            let data_details =[[{text:`#`,fontSize: 7,alignment:'center'},
            {text:'รหัสสินค้า',alignment:'center',fontSize: 7},
            {text:`ชื่อสินค้า`,alignment:'center',fontSize: 7},
            {text:'QTY',alignment:'center',fontSize: 7}]]
            let i = 1
            let total = 0
            order_details.rows.forEach((element)=>{
                data_details.push([{text:i,alignment:'center',fontSize: 7}
                    ,{text:element.promotion_code,alignment:'center',fontSize: 7}
                    ,{text:element.promotion_name,fontSize: 7}
                    ,{text:element.qty_promotion,alignment:'center',fontSize: 7}])
                i++
                total += parseInt(element.qty_promotion)
            })
            docDefinition.content.push({
                style: 'detail',margin:[ 0,2,0,2 ],
                layout: {
                    hLineWidth: function (i, node) {
                        return (i === 1 || i === 0 || i === data_details.length) ? 1 : 0;
                    },
                    vLineWidth: function (i, node) {
                    return (i === 3 || i === 4 || i === 0) ? 1 : 0;
                    }
                },
                table:{
                    widths: ['5%','15%','70%','10%'],
                    heights: 10,
                    headerRows:1,
                    body: data_details
                }
            })
            if((main+1) != order_number.length){
                docDefinition.content.push({
                    style: 'tableExample',
                    table: {
                        widths: [23,23],
                        heights: 'auto',
                        body: [
                            [{text:'รวม',alignment:'center',fontSize: 14}, {text:total,alignment:'center',fontSize: 14}],
                        ]
                    },margin: [208,0],pageBreak: `after`
                })
            }else{
                docDefinition.content.push({
                    style: 'tableExample',
                    table: {
                        widths: [23,23],
                        heights: 'auto',
                        body: [
                            [{text:'รวม',alignment:'center',fontSize: 14}, {text:total,alignment:'center',fontSize: 14}],
                        ]
                    },margin: [208,0]
                })
            }
            
        }

        var options = {
        }

        var pdfDoc = printer.createPdfKitDocument(docDefinition, options);
        pdfDoc.pipe(res);
        pdfDoc.end();
    }
    
}