const { lish_member } = require("../../config/Database")

exports.whctl = class wh{

    async view(req,res){
        const shop_code = req.params.shop_code
        let item = await lish_member.query(`select *,sp.id as id,sp.status as status from shop_product sp left join lish_product_user lpu on sp.product_code = lpu.product_code_user where shop_code = '${shop_code}'`)
        let shop = await lish_member.query(`select * from setting_shop where shop_code = '${shop_code}'`)
        let item_active = await lish_member.query(`select * from shop_product where shop_code = '${shop_code}' and status = 1`)
        res.render('./wh/warehouse',{item:item.rows,noti:shop.rows[0].noti_stock,item_acitve:item_active.rows.length})
    }

    async add(req,res){
        const {id,amount} = req.body
        let item = await lish_member.query(`select * from shop_product where shop_code = '${req.user.shop_code}' and id = ${id}`)
        await lish_member.query(`update shop_product set quantity=${parseInt(item.rows[0].quantity)+parseInt(amount)} where shop_code = '${req.user.shop_code}' and id = ${id}`,async (err,result)=>{
            if(err){
                console.log(err)
                res.send({Message:'Error'})
            }else{
                let event = await lish_member.query(`INSERT INTO shop_event_wh 
                (channel, type_channel, quantity, shop_code, remak, "date",product_name,product_code) 
                VALUES('ADD_STOCK', '+', ${amount}, '${req.user.shop_code}', '', now(),'${item.rows[0].product_name}','${item.rows[0].product_code}');`)
                res.send({Message:'Success'})
            }
        })
    }

    async delete_wh(req,res){
        const {id,amount,remak} = req.body
        let item = await lish_member.query(`select * from shop_product where shop_code = '${req.user.shop_code}' and id = ${id}`)
        if((parseInt(item.rows[0].qty_book)+parseInt(amount)) > item.rows[0].quantity){
            res.send({Message:'สินค้าไม่เพียงพอ'})
        }else{
            await lish_member.query(`update shop_product set qty_book=${parseInt(item.rows[0].qty_book)+parseInt(amount)} where shop_code = '${req.user.shop_code}' and id = ${id}`,async (err,result)=>{
                if(err){
                    console.log(err)
                    res.send({Message:'Error'})
                }else{
                    let event = await lish_member.query(`INSERT INTO shop_event_wh 
                    (channel, type_channel, quantity, shop_code, remak, "date",product_name,product_code) 
                    VALUES('REQUISITION', '-', ${amount}, '${req.user.shop_code}', '${remak}', now(),'${item.rows[0].product_name}','${item.rows[0].product_code}');`)
                    res.send({Message:'Success'})
                }
            })
        }
    }

    async change(req,res){
        const {id,status} = req.body
        let item = await lish_member.query(`select * from shop_product where shop_code = '${req.user.shop_code}' and id = ${id}`)
        let promotion = await lish_member.query(`select promotion_code from lish_promotion_detail where product_code = '${item.rows[0].product_code}'`)
        let check = true
        let update_product = await lish_member.query(`update shop_product set status=${status} where shop_code = '${req.user.shop_code}' and id = ${id}`)
        if(status == 0){
            for(let i = 0;i < promotion.rows.length;i++){
                await lish_member.query(`update shop_promotion set status=${status} where promotion_code = '${promotion.rows[i].promotion_code}' and shop_code = '${req.user.shop_code}'`,async (err,result)=>{
                    if(err){
                        console.log(err)
                        check = false
                    }
                })
            }
        }
        if(check){
            res.send({Message:'Success'})
        }else{
            res.send({Message:'Error'})
        }
    }

    async noti(req,res){
        const {amount} = req.body
        await lish_member.query(`update setting_shop set noti_stock = ${amount} where shop_code = '${req.user.shop_code}'`,async (err,result)=>{
            if(err){
                console.log(err)
                res.send({Message:'Error'})
            }else{
                res.send({Message:'Success'})
            }
        })
    }

    async view_event(req,res){
        let event = await lish_member.query(`select *,to_char(date,'yyyy-MM-dd HH:mm:ss') as date from shop_event_wh sew where shop_code = '${req.user.shop_code}'`)
        res.render('./wh/event_wh' ,{event:event.rows})
    }
}