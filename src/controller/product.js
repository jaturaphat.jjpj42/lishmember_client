const {oneoms,whoms,lish_member,select_lish} = require('../../config/Database')
const {format} = require('date-fns')
const multer =require("multer")
const crypto = require("crypto");
const fs = require("fs")
const Noti = require("../../config/LineNoti");
const SendNoti = require('../../config/LineNoti');
require('dotenv').config()

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

var storage1 = multer.diskStorage({
    destination: function (req, file, callback) {
    var dir = "./src/public/image/product";
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    callback(null, dir);
    },
    filename: function (req, file, callback) {
    let randomname = makeid(5)
    let name = file.originalname
    let name_data = name.substring(0,3)
    let datetime = format(new Date,"yyyyMMddHHmmss")
    callback(null, `${name_data}${randomname}-${datetime}.jpg`);
    },
});

var uploadpicturefields = multer({ storage: storage1 }).fields([{name:"file1"},{name:"file2"},{name:"file3"},{name:"file4"}]);
var uploadpictureArray= multer({ storage: storage1 }).array("files");

exports.product = class product{

    async view(req,res){
        let data = await lish_member.query('select *,mr.roles_name,lp.id as id from lish_product lp left join member_roles mr on lp.id_roles = mr.id order by lp.price DESC')
        res.render('./product/Product' , {data:data.rows})
    }

}

exports.add_order = class add_order{

    async views(req,res){
        let id = req.params.id
        let check_count = await lish_member.query(`SELECT max(order_number) from member_order where DATE(order_date) = DATE(now())`)
        let yearsnow = format(new Date(),'yyMMdd')
        let code_text = `0`
        if(check_count.rows[0].max != null){
            code_text = (check_count.rows[0].max).substring(9)
            code_text = parseInt(code_text)+1
        }else{
            code_text = `0001`;
        }
        
        let code = 'MEM'+ yearsnow + (String(parseInt(code_text)).padStart(3, '0'))
        let data = await lish_member.query(`select *,lp.id as id,mr.roles_name from lish_product lp left join member_roles mr on lp.id_roles = mr.id where lp.id = ${id}`)
        let session = req.session
        session.product = data.rows[0]
        session.code = code
        res.render('./product/linklish',{Product:data.rows[0],code:code})
    }

    async insert_order(req,res){
        uploadpicturefields(req,res,async function (err) {
            if(err){
                console.log(err)
            }else{
                let session = req.session
                let data = session.data
                // let member = await select_lish(`select * from setting_shop ss left join member_lish ml on ss.shop_code = ml.shop_code where ss.shop_code = '${data.shop_code}'`)
                let member = await select_lish(`select * from member_lish ml where email = '${req.user.email}'`)
                let product = await select_lish(`select * from lish_product lp where lp.id = ${data.product_code}`)
                let address = {}
                if(data.type == 2){
                    address = {
                        subdistrict:data.subdistrict,
                        district:data.district,
                        province:data.province,
                        zipcode:data.zipcode,
                        address:data.address,
                    }
                }else{
                    address ={
                        subdistrict:member[0].subdistrict,
                        district:member[0].district,
                        province:member[0].province,
                        zipcode:member[0].zipcode,
                        address:member[0].address,
                    }
                }
                let img
                let order_status = 0
                if(data.option == 1){
                    img = `'/image/product/${req.files.file1[0].filename}'`
                    order_status = 1
                }else{
                    img = null
                    order_status = 1
                }
                data.shop_code = req.user.shop_code
                await lish_member.query(`INSERT INTO member_order 
                (order_number, first_name, last_name, tel, email, address, subdistrict, district, province, zipcode
                , payment, payment_status, bank, images, shop_code, order_status ,order_date) 
                VALUES('${data.code}', '${member[0].first_name}', '${member[0].last_name}', '${member[0].tel}', '${member[0].email}'
                , '${address.address}', '${address.subdistrict}', '${address.district}', '${address.province}', '${address.zipcode}'
                , ${data.option}, 0, 0
                , ${img}, '${data.shop_code}', ${order_status} , Date(now()))`,async (err,result)=>{
                    if(err){
                        console.log(err)
                        res.send({Message:'Error_1'})
                    }else{
                        await lish_member.query(`INSERT INTO member_order_detail 
                        (order_number, shop_code, product_code_oms, quantity, price, order_date ,product_code_wh) 
                        VALUES('${data.code}', '${data.shop_code}', '${product[0].product_code_oms}', 1, ${product[0].price}, DATE(now()), '${product[0].product_code_wh}');
                        `,async (err,element)=>{
                            if(err){
                                console.log(err)
                                res.send({Message:'Error_2'})
                            }else{
                                let shop = await lish_member.query(`select * from setting_shop where shop_code ='${data.shop_code}'`)
                                if(shop.rows.length != 0){
                                    let msg = `มีคำสั่งซื้อจากร้านค้า ${shop.rows[0].shop_name}`
                                    SendNoti(msg,'kPayLOdXnaSVfdjvixMB4d1oCWdGbNHyk4ce6xRo55M',(err,result)=>{})
                                    res.send({Message:'Success'})
                                }else{
                                    let msg = `มีคำสั่งซื้อจากสมาชิก ${member[0].first_name} ${member[0].last_name}`
                                    SendNoti(msg,'kPayLOdXnaSVfdjvixMB4d1oCWdGbNHyk4ce6xRo55M',(err,result)=>{})
                                    res.send({Message:'Success'})
                                }
                            }
                        })
                    }
                })
            }
        }) 
    }

    async view_Thank(req,res){
        let session = req.session
        let data_session = session.data
        let id = data_session.code
        let product = data_session.product_code
        let member = await lish_member.query(`select * from member_order where order_number = '${id}'`)
        let data = await lish_member.query(`select *,lp.id as id,mr.roles_name from lish_product lp left join member_roles mr on lp.id_roles = mr.id where lp.id = ${product}`)
        res.render('./product/linklishthankyou',{Product:data.rows[0],code:id,member:member.rows[0]})
    }

    async view_confirm(req,res){
        let session = req.session
        let data_session = {}
        // console.log(req.body)
        if(req.body.option1 == '1'){
            data_session.option = 1
        }else{
            data_session.option = 2
        }
        data_session.code = req.body.code
        data_session.product_code = req.body.product_code
        data_session.shop_code = req.body.shop_code
        data_session.address = req.body.address
        data_session.subdistrict = req.body.subdistrict
        data_session.district = req.body.district
        data_session.province = req.body.province
        data_session.zipcode = req.body.zipcode
        if(req.body.zipcode != ''){data_session.type = 2}
        else{data_session.type = 1}
        session.data = data_session
        if(data_session.option == 1){
            res.redirect('/product/linkLish/tranfer')
        }else{
            res.redirect('/product/epayment')
        }
        
    }

    async view_upload(req,res){
        uploadpicturefields(req,res,async function (err) {
            if(err){
                console.log(err)
            }else{
                let session = req.session
                let data_session = session.data
                let id = data_session.code
                let product = data_session.product_code
                let data = await lish_member.query(`select *,lp.id as id,mr.roles_name from lish_product lp left join member_roles mr on lp.id_roles = mr.id where lp.id = ${product}`)
                res.render('./product/linklishconfirm' ,{Product:data.rows[0],code:id})
            }
        })
    }

    async epayment(req,res){
        let session = req.session
        let data = session.data
        data.order_number = session.data.code
        // console.log(data)

        let price_order = await lish_member.query(`select * from lish_product where id = '${data.product_code}'`)
        var hmac = crypto.createHmac('sha256','QnmrnH6QE23N'); 
        // QnmrnH6QE23N
        // JT04
        let merchant_id = 'JT04';          //Get MerchantID when opening account with 2C2P
        let url = process.env.URL
        let payment_description =  `${data.code}`;
        let order_id = data.code;
    
        let currency = "764";
        var digit = '0000000000';
        var price = digit+price_order.rows[0].price+'00';
        var totalPrice = price.substr(-12, 12);    
        let amount = totalPrice;   
                                        
        //Request information
        let version = "8.5";  
        // var ipp_interest_type = 'A';
        // var ipp_period_filter = "3"; 
        var payment_option = "ALL";//CC
        let payment_url = "https://t.2c2p.com/RedirectV3/payment";  
        // let resultUrl1 = `https://affiliate.awbg.co.th/order/thank?order_number=${order_id}`; 
        // let resultUrl1 = `https://affiliateuat.awbg.co.th/order/${orders[0].product_model}/thank`;    
        // let resultUrl1 = `https://affiliateuat.awbg.co.th/order/thank?order_number=${order_id}`; 
        // let resultUrl2 = `https://affiliateuat.awbg.co.th/api/v1/callback`; 
        let resultUrl1 = `http://128.199.100.251:1001/api/insert_order`;    
        let resultUrl2 = `http://128.199.100.251:1001/product/linkLish/tranfer`;
                                
        //Construct signature string
        var params = version+merchant_id+payment_description+order_id+currency+amount+resultUrl1+resultUrl2+payment_option; //+ipp_interest_type+ipp_period_filter , ipp_interest_type:ipp_interest_type, ipp_period_filter:ipp_period_filter

        var datahex = hmac.update(params);
        var gen_hmac= datahex.digest('hex');
        res.render('./product/payment',{data:data,version:version,merchant_id:merchant_id,currency:currency,resultUrl1:resultUrl1,resultUrl2:resultUrl2,hash_value:gen_hmac,payment_description:payment_description,order_id:order_id,amount:amount,payment_option:payment_option});
    }

    async test(req,res){
        res.render('./product/test')
    }
}

exports.promotion = class promotion{

    async views(req,res){
        let shop_code = req.params.id
        let data = await lish_member.query(`select *,sp.status as status,lp.shop_code as shop_code from shop_promotion sp left join lish_promotion lp on sp.promotion_code = lp.promotion_code where sp.shop_code = '${shop_code}' and lp.on_delete = 'N';`)
        data.rows.forEach(element=>{
            let img = JSON.parse(element.img_link)
            element.img_link = img[0].img
        })
        // console.log(req.user)
        let shop_product = await lish_member.query(`select * from shop_product where shop_code = '${shop_code}' and status = 1`)
        let promotion = await lish_member.query(`select * from lish_promotion`)
        let promotion_detail = await lish_member.query(`select * from lish_promotion_detail`)
        res.render(`./product/promotion`, {data:data.rows,promotion:promotion.rows,promotion_detail:promotion_detail.rows,shop_product:shop_product.rows})
    }

    async swtich_status(req,res){
        const {status,code,shop_code} = req.body
        console.log(status,code,shop_code)
        await lish_member.query(`update shop_promotion set status = ${status} where promotion_code = '${code}' and shop_code = '${shop_code}'`,
        async (err,result)=>{
            if(err){
                console.log(err)
                res.send({Message:'Error'})
            }else{
                res.send({Message:'Success'})
            }
        })
    }

    async views_add(req,res){
        let product = await lish_member.query(`select * from lish_product_user`)
        let check_count = await lish_member.query(`SELECT max(promotion_code) from lish_promotion where promotion_code like '%PM%'`)
        let yearsnow = format(new Date(),'yyMM')
        let code_text = `0`
        if(check_count.rows[0].max != null){
            code_text = (check_count.rows[0].max).substring(6)
            code_text = parseInt(code_text)+1
        }else{
            code_text = `001`;
        }
        let promotion = {id:0}
        let code = 'PM'+ yearsnow + (String(parseInt(code_text)).padStart(2, '0'))
        res.render('./product/add_promotion',{code:code,product:product.rows,promotion:promotion,promotion_detail:[]})
    }

    async views_update(req,res){
        const id = req.params.id
        let product = await lish_member.query(`select * from lish_product_user`)
        let promotion = await lish_member.query(`select *,to_char(start_date,'yyyy-MM-dd') as start_date,to_char(end_date,'yyyy-MM-dd') as end_date from lish_promotion where promotion_code = '${id}'`)
        let promotion_detail = await lish_member.query(`select * from lish_promotion_detail where promotion_code = '${id}'`)
        promotion.rows[0].img_link != '' ? promotion.rows[0].img_link = JSON.parse(promotion.rows[0].img_link) : ''
        res.render(`./product/add_promotion` ,{code:id,product:product.rows,promotion:promotion.rows[0],promotion_detail:promotion_detail.rows})
    }

    async insert_promotion(req,res){
        uploadpictureArray(req,res,async function (err) {
            if(err){
                console.log(err)
            }else{
                let img = []
                if(req.files.length != 0){
                    req.files.forEach(element => {
                        img.push({img:`image/product/${element.filename}`})
                    });
                }
                let img_json = JSON.stringify(img)
                let data = req.body
                data.end == '' ? data.end = null : data.end = `'${data.end}'`
                await lish_member.query(`INSERT INTO lish_promotion 
                (promotion_code, promotion_name, description, price, start_date, end_date, status, img_link, price_sale,shop_code) 
                VALUES('${data.promotion_code}', '${data.promotion_name}', '${data.description}', ${data.price}, '${data.start}', ${data.end}, 0, '${img_json}', ${data.sale_price}, '${data.shop_code}');`
                ,async (err,result)=>{
                    if(err){
                        console.log(err,'Error_1')
                        res.send({Message:'Error'})
                    }else{
                        let insert_shop = await lish_member.query(`insert into shop_promotion (promotion_code,promotion_name,status,shop_code) 
                        values ('${data.promotion_code}', '${data.promotion_name}', 0 ,'${data.shop_code}')`)
                        if(parseInt(data.count) > 1){
                            for(let count = 0;count < data.count;count++){
                                await lish_member.query(`INSERT INTO lish_promotion_detail 
                                (promotion_code, product_code, product_name, quantity) 
                                VALUES('${data.promotion_code}', '${data.product_code[count]}', '${data.product_name[count]}', ${data.quantity[count]});`,async (err,result)=>{
                                    if(err){
                                        console.log(err,'Error_2')
                                        res.send({Message:'Error'})
                                    }
                                })
                            }
                        }else{
                            await lish_member.query(`INSERT INTO lish_promotion_detail 
                            (promotion_code, product_code, product_name, quantity) 
                            VALUES('${data.promotion_code}', '${data.product_code}', '${data.product_name}', ${data.quantity});`,async (err,result)=>{
                                if(err){
                                    console.log(err,'Error_2')
                                    res.send({Message:'Error'})
                                }
                            })
                        }
                        Noti(`\nPromotion : ${data.promotion_name}\nStatus : รอการอนุมัติ`,process.env.line_noti,(err,result)=>{
                            if(err){
                                console.log('Error_Noti',err)
                                res.send({Message:'Error'})
                            }else{
                                res.send({Message:'Success'})
                            }
                        })
                    }
                })
            }
        })
    }

    async update_promotion(req,res){
        uploadpictureArray(req,res,async function (err) {
            if(err){
                console.log(err)
            }else{
                let img = []
                if(req.files.length != 0){
                    req.files.forEach(element => {
                        img.push({img:`image/product/${element.filename}`})
                    });
                }
                let img_json
                let data = req.body
                let data_in_db = await lish_member.query(`select * from lish_promotion where promotion_code = '${data.promotion_code}'`)
                if(img.length != 0){
                    let img_old = JSON.parse(data_in_db.rows[0].img_link)
                    console.log((img_old.length + img.length > 5))
                    if(img_old.length + img.length > 5){
                        img_old.forEach(element=>{
                            fs.unlinkSync(`./src/public/product/${element.img}`)
                        })
                    }else{
                        img_old.forEach(element=>{
                            img.push({img:element.img})
                        })
                    }
                    img_json = JSON.stringify(img)
                }else{
                    img_json = data_in_db.rows[0].img_link
                }
                data.end == '' ? data.end = null : data.end = `'${data.end}'`
                await lish_member.query(`update lish_promotion set
                promotion_name ='${data.promotion_name}', description ='${data.description}', price=${data.price}
                , start_date='${data.start}', end_date=${data.end}, status=0, img_link='${img_json}', price_sale=${data.sale_price} where promotion_code = '${data.promotion_code}' and shop_code ='${data.shop_code}'`
                ,async (err,result)=>{
                    if(err){
                        console.log(err,'Error_1')
                        res.send({Message:'Error'})
                    }else{
                        let update_product = await lish_member.query(`update shop_promotion set promotion_name='${data.promotion_name}',status=0 where promotion_code = '${data.promotion_code}' and shop_code ='${data.shop_code}'`)
                        let delete_detail = await lish_member.query(`delete from lish_promotion_detail where promotion_code = '${data.promotion_code}'`)
                        if(parseInt(data.count) > 1){
                            for(let count = 0;count < data.count;count++){
                                await lish_member.query(`INSERT INTO lish_promotion_detail 
                                (promotion_code, product_code, product_name, quantity) 
                                VALUES('${data.promotion_code}', '${data.product_code[count]}', '${data.product_name[count]}', ${data.quantity[count]});`,async (err,result)=>{
                                    if(err){
                                        console.log(err,'Error_2')
                                        res.send({Message:'Error'})
                                    }
                                })
                            }
                        }else{
                            await lish_member.query(`INSERT INTO lish_promotion_detail 
                            (promotion_code, product_code, product_name, quantity) 
                            VALUES('${data.promotion_code}', '${data.product_code}', '${data.product_name}', ${data.quantity});`,async (err,result)=>{
                                if(err){
                                    console.log(err,'Error_2')
                                    res.send({Message:'Error'})
                                }
                            })
                        }
                        Noti(`\nPromotion : ${data.promotion_name}\nStatus : รอการอนุมัติ (แก้ไข)`,process.env.line_noti,(err,result)=>{
                            if(err){
                                console.log('Error_Noti',err)
                                res.send({Message:'Error'})
                            }else{
                                res.send({Message:'Success'})
                            }
                        })
                    }
                })
            }
        })
    }

    async delete_promotion(req,res){
        let promotion_code = req.body.code
        await lish_member.query(`update lish_promotion set status = 0,on_delete = 'Y' where promotion_code = '${promotion_code}'`,async (err,result)=>{
            if(err){
                console.log(err)
                res.send({Message:'Error'})
            }else{
                let update = await lish_member.query(`update shop_promotion set status = 0 where promotion_code = '${promotion_code}' and shop_code = '${req.user.shop_code}'`)
                res.send({Message:'Success'})
            }
        })
    }
}