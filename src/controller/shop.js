const {oneoms,whoms,lish_member} = require('../../config/Database')
const multer = require('multer')
const fs = require('fs')
const {format } = require("date-fns")
const request = require('request')
const sessions = require('express-session')
const crypto = require('crypto')
var data = []
var cart = []
var data_credit
var cart_credit
var total = 0
var text = '';
var session;
var Order_name
function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

var storage1 = multer.diskStorage({
    destination: function (req, file, callback) {
    var dir = "./src/public/image/order";
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    callback(null, dir);
    },
    filename: function (req, file, callback) {
    let randomname = makeid(5)
    let name = file.originalname
    let name_data = name.substring(0,3)
    let datetime = format(new Date,"yyyyMMddHHmmss")
    callback(null, `${name_data}${randomname}-${datetime}.jpg`);
    },
});

var uploadpicturefields = multer({ storage: storage1 }).single('ImgBankTransfer');


exports.shop = class shop{
    async views(req,res){
        let {shop} = req.query
        //console.log(shop);
        let ShopId =  await lish_member.query(`Select * from setting_shop where shop_code = '${shop}'`)
        let product_promotion = await lish_member.query(`Select *,sp.status as status from shop_promotion sp left join lish_promotion lp on sp.promotion_code = lp.promotion_code where sp.shop_code = '${shop}' and sp.status = 1`)
        product_promotion.rows.forEach(element=>{
            let image_promotion = JSON.parse(element.img_link)
            element.img_link = image_promotion
        })
        session = req.session;
        session.name = data.Name;
        session.NumberPhone = data.NumberPhone;
        session.Address = data.Address;
        session.district = data.district;
        session.amphoe = data.amphoe;
        session.province = data.province;
        session.postcode = data.postcode;
        session.Email = data.Email;
        session.count += 1;
        //console.log(session.count);
        let DataSession = {
            Name : session.name,
            NumberPhone : session.NumberPhone,
            Address : session.Address,
            District : session.district,
            Amphoe : session.Amphoe,
            Province : session.Province,
            Postcode : session.Postcode,
            Email : session.Email
        }
        res.render('./shop/shop',{Count:session.Count,Data_promotion:product_promotion.rows,DataSession:DataSession,Status_credit:ShopId.rows[0].status_credit,Status_Bank:ShopId.rows[0].status_transfer,Status_cod:ShopId.rows[0].status_cod,ShopName:ShopId.rows[0].shop_name,Img_pf:ShopId.rows[0].image_profile,Img_bg:ShopId.rows[0].image_bg,Shop_code:shop})
    }
    
    async getData(req,res){
        let {shop} = req.query
        let creativestore = await lish_member.query(`SELECT * FROM shop_creative where shop_code = '${shop}'`)
        let product_promotion = await lish_member.query(`Select *,sp.status as status from shop_promotion sp left join lish_promotion lp on sp.promotion_code = lp.promotion_code where sp.shop_code = '${shop}' and sp.status = 1`)
        product_promotion.rows.forEach(element=>{
            let image_promotion = JSON.parse(element.img_link)
            element.img_link = image_promotion
        })
        //console.log(creativestore.rows);
        res.send({creative:creativestore.rows,product: product_promotion.rows})
    }

    async productdetail(req,res){
        let {id,shop} = req.query;
        let show = []
        let product_details_promotion = await lish_member.query(`Select *,sp.status as status from shop_promotion sp left join lish_promotion lp on sp.promotion_code = lp.promotion_code where sp.shop_code = '${shop}' and sp.status = 1 and sp.promotion_code != '${id}'`)
        let data_product_details = await lish_member.query(`select * from lish_promotion where promotion_code = '${id}'`)
        let ShopId =  await lish_member.query(`Select * from setting_shop where shop_code = '${shop}'`)
        // console.log(product_details_promotion.rows,'test1');
        // console.log(data_product_details.rows,'test2');
        // console.log(ShopId.rows,'test3');
        session = req.session;
        session.name = data.Name;
        session.NumberPhone = data.NumberPhone;
        session.Address = data.Address;
        session.district = data.district;
        session.amphoe = data.amphoe;
        session.province = data.province;
        session.postcode = data.postcode;
        session.Email = data.Email;
        let DataSession = {
            Name : session.name,
            NumberPhone : session.NumberPhone,
            Address : session.Address,
            District : session.district,
            Amphoe : session.Amphoe,
            Province : session.Province,
            Postcode : session.Postcode,
            Email : session.Email
        }
        data_product_details.rows.forEach(element=>{
            let image_promotion = JSON.parse(element.img_link)
            element.img_link = image_promotion
        })
        product_details_promotion.rows.forEach(element=>{
            let image_details_promotion = JSON.parse(element.img_link)
            element.img_link = image_details_promotion
        })
        
        if(product_details_promotion.rows.length <= 3){
            for( let i = 0; i < product_details_promotion.rows.length; i++){
                show.push(product_details_promotion.rows[i])
            }
        }else{
            while (show.length < 3 ) {
                let id = Math.floor(Math.random() * product_details_promotion.rows.length)
                let check = show.filter(value=>product_details_promotion.rows[id].promotion_code==value.promotion_code)
                if(check.length == 0){
                    show.push(product_details_promotion.rows[id])
                }
                //console.log(show);
            }
        }
        let detail = data_product_details.rows[0].description
        detail = JSON.stringify(detail)
        res.render('./shop/productdetail',{Shop_code:shop,Product_details : data_product_details.rows[0],Product_Other:show,DataSession:DataSession,Status_credit:ShopId.rows[0].status_credit,Status_Bank:ShopId.rows[0].status_transfer,Status_cod:ShopId.rows[0].status_cod,ShopName:ShopId.rows[0].shop_name,Img_pf:ShopId.rows[0].image_profile,Detail:detail})
    }
    keep_info(req,res){
        let Data_emp = req.body.Data;
        let Data_cart = req.body.Cart;
        session = req.session;
        session.information = data
        session.information_cart = cart
        session.Data = Data_emp;
        session.Cart = Data_cart;
        data_credit = Data_emp
        cart_credit = Data_cart
        // console.log(Data_emp);
        res.send({Message:"Success"})
    }
    async confirm_order(req,res){
        // console.log(total);
        total = 0 
        let TotalQty = 0
        let Cart = req.session.Cart;
        let Data = req.session.Data;
        let Shop_code = Data.Shop;
        for(let i = 0 ; i < Cart.length ; i++){
            total += Cart[i].quantity * Cart[i].price
            TotalQty += Cart[i].quantity
        }
        if(Data.COD == 1 || Data.BANK ==1){
            let ShopBank = await  lish_member.query(`Select * from setting_shop_transfer sst left join setting_bank sb on sst.bank = sb.bank_code where sst.shop_code = '${Shop_code}'`)
            // console.log(ShopBank.rows);
            // let ImageBank = await lish_member.query(`Select bank_image from setting_bank where bank_code = ${}`)
            let color = [] ;
            let IdBtnBank = [];
            for(let i = 0 ; i < ShopBank.rows.length; i ++){
                if(ShopBank.rows[i].bank == 'bbl'){
                    color.push("style=background-color:#1e4598;color:white;")
                }else if(ShopBank.rows[i].bank == 'kbank'){
                    color.push("style=background-color:#138f2d;color:white;")
                }else if(ShopBank.rows[i].bank == 'bay'){
                    color.push("style=background-color:#fec43b;color:white;")
                }else if (ShopBank.rows[i].bank == 'ktb'){
                    color.push("style=background-color:#0093D9;color:white;")
                }else if(ShopBank.rows[i].bank == 'kk'){
                    color.push("style=background-color:#199cc5;color:white;")
                }else if(ShopBank.rows[i].bank == 'cimb'){
                    color.push("style=background-color:#7e2f36;color:white;")
                }else if(ShopBank.rows[i].bank == 'ttb'){
                    color.push("style=background-image:linear-gradient(blue,orange);color:white;")
                }else if(ShopBank.rows[i].bank == 'scb'){
                    color.push("style=background-color:#4e2e7f;color:white;")
                    IdBtnBank.push(`id=${ShopBank.rows[i].bank}`)
                }else if(ShopBank.rows[i].bank == 'uob'){
                    color.push("style=background-color:#0b3979;color:white;")
                }else if(ShopBank.rows[i].bank == 'lhb'){
                    color.push("style=background-color:#6d6e71;color:white;")
                }else if(ShopBank.rows[i].bank == 'sc'){
                    color.push("style=background-color:#0f6ea1;color:white;")
                }else if(ShopBank.rows[i].bank == 'icbc'){
                    color.push("style=background-color:#c50f1c;color:white;")
                }else if(ShopBank.rows[i].bank == 'tisco'){
                    color.push("style=background-color:#12549f;color:white;")
                }
                else{
                    color.push("style=background-color:orange;color;white")
                }
            }
            console.log(Data);
            console.log(Cart);
            res.render('./shop/confirm_order',{Shop_code:Shop_code,Data:Data,Cart:Cart,ShopBank:ShopBank.rows,Color:color,IdBtnBank:IdBtnBank,TotalQty:TotalQty,Total:total})
        }else if(Data.CREDIT == 1){
            Order_name = ''
            let check_count = await lish_member.query(`SELECT order_number from lish_orders where DATE(order_date) = DATE(now()) and shop_code = '${Data.Shop}' ORDER by id desc limit 1`)
            let yearsnow = format(new Date(),'yyyyMMdd')
            let shop_id = await lish_member.query(`SELECT id FROM setting_shop where shop_code = '${Data.Shop}'`)
            let shop = `${parseInt('0000')+parseInt(shop_id.rows[0].id)}`
            let tel = Data.NumberPhone.slice(-3)
            let code_text = 0
            if(check_count.rows.length != 0){
                code_text = (check_count.rows[0].order_number).substring(14, 18)
                code_text = parseInt(code_text)+1
            }else{
                code_text = `0001`;
            }
            let code = `L${String(parseInt(shop)).padStart(4, '0')}-${yearsnow}${String(parseInt(code_text)).padStart(4, '0')}${tel}`
            var hmac = crypto.createHmac('sha256','QnmrnH6QE23N'); 
            Order_name = code
            // QnmrnH6QE23N
            // JT04
            let merchant_id = 'JT04';          //Get MerchantID when opening account with 2C2P
            let url = process.env.URL
            let order_id = `${Order_name}`;
            let payment_description =  `${Order_name}`;
            let currency = "764";
            var digit = '0000000000';
            // console.log(data.TotalPrice);
            var price = digit+`${total}`+'00';
            var totalPrice = price.substr(-12, 12);    
            let amount = totalPrice;  
            // console.log(amount);
                                            
            //Request information
            let version = "8.5";  
            // var ipp_interest_type = 'A';
            // var ipp_period_filter = "3"; 
            var payment_option = "ALL";//CC
            let payment_url = "https://t.2c2p.com/RedirectV3/payment";  
            // let resultUrl1 = `https://affiliate.awbg.co.th/order/thank?order_number=${order_id}`; 
            // let resultUrl1 = `https://affiliateuat.awbg.co.th/order/${orders[0].product_model}/thank`;    
            // let resultUrl1 = `https://affiliateuat.awbg.co.th/order/thank?order_number=${order_id}`; 
            // let resultUrl2 = `https://affiliateuat.awbg.co.th/api/v1/callback`; 
            let resultUrl1 = `http://128.199.100.251:1001/shop/buyorder3`;    
            let resultUrl2 = `http://128.199.100.251:1001/shop/buyorder3`;  
            //Construct signature string
            var params = version+merchant_id+payment_description+order_id+currency+amount+resultUrl1+resultUrl2+payment_option; //+ipp_interest_type+ipp_period_filter , ipp_interest_type:ipp_interest_type, ipp_period_filter:ipp_period_filter
            var datahex = hmac.update(params);
            var gen_hmac= datahex.digest('hex');
            res.render('./shop/epayment',{version:version,merchant_id:merchant_id,currency:currency,resultUrl1:resultUrl1,resultUrl2:resultUrl2,hash_value:gen_hmac,payment_description:payment_description,order_id:order_id,amount:amount,payment_option:payment_option,Data:Data,Cart:Cart,TotalQty:TotalQty,Total:total})
            //res.render('./shop/epayment',{Data:data,Cart:cart})
        }
    }
    async buyorder(req,res){
        var id = req.params.id;
        var Cart = req.session.Cart;
        //console.log(Shop_code,Data,Cart);
        if(id == 1){
            // console.log(arr);
            uploadpicturefields(req,res,async function (err) {
                if(err){
                    console.log(err);
                }else{
                    //รอเพิ่มเข้า Database 
                    var newInfo = req.body
                    let datetime = format(new Date,"yyyy-MM-dd HH:mm:ss")
                    let check_count = await lish_member.query(`SELECT order_number from lish_orders where DATE(order_date) = DATE(now()) and shop_code = '${newInfo.Shop}' ORDER by id desc limit 1`)
                    let shop_id = await lish_member.query(`SELECT id FROM setting_shop where shop_code = '${newInfo.Shop}'`)
                    let yearsnow = format(new Date(),'yyyyMMdd')
                    let shop = `${parseInt('0000')+parseInt(shop_id.rows[0].id)}`
                    let tel = newInfo.numberphone.slice(-3)
                    let code_text = 0
                    if(check_count.rows.length != 0){
                        code_text = (check_count.rows[0].order_number).substring(14, 18)
                        code_text = parseInt(code_text)+1
                    }else{
                        code_text = `0001`;
                    }
                    let arr = []
                    arr.push({img:`image/order/${req.file.filename}`})
                    let newJson = JSON.stringify(arr)
                    let code = `L${String(parseInt(shop)).padStart(4, '0')}-${yearsnow}${String(parseInt(code_text)).padStart(4, '0')}${tel}`
                    Order_name = code
                    await lish_member.query(`INSERT INTO public.lish_orders
                    (order_number, first_name, last_name,tel, tel2, email, address, subdistrict, district, province, zipcode, payment, payment_status, delivery, delivery_status, courier_code, courier_tracking, courier_booking, bank, images, marketplace_tracking, social, reason, order_date, price, discount, freight, total, order_status,shop_code)
                    VALUES('${code}',
                    '${newInfo.fname}', 
                    '${newInfo.lname}',
                    '${newInfo.numberphone}',
                    '', 
                    '${newInfo.email}', 
                    '${newInfo.address}', 
                    '${newInfo.district}', 
                    '${newInfo.amphoe}',
                    '${newInfo.province}',
                    '${newInfo.postcode}',
                    ${parseInt(newInfo.payment)},
                    ${parseInt(newInfo.payment_status)},
                    0,
                    ${parseInt(newInfo.delivery_status)},
                    '${newInfo.courier_code}',
                    '${newInfo.courier_tracking}',
                    '${newInfo.courier_booking}',
                    ${parseInt(newInfo.bank)},
                    '${newJson}',
                    '', 
                    '12',
                    '',
                    '${datetime}',
                    ${total},
                    0,
                    0,
                    ${total},
                    1,
                    '${newInfo.Shop}');`)
                    for(let i = 0 ; i < Cart.length ; i++){
                        let lish_orders = await lish_member.query(`select * from lish_promotion_detail lpd left join lish_product_user lpu on lpd.product_code = lpu.product_code_user where lpd.promotion_code = '${Cart[i].id}' `)
                        let quantity = 0
                        let total = 0
                        for(let j = 0 ; j < lish_orders.rows.length ; j++){
                            // console.log(cart[i].id);
                            // console.log(lish_orders.rows[j].promotion_code);
                            if(lish_orders.rows[j].promotion_code == Cart[i].id){
                                quantity = parseInt(lish_orders.rows[j].quantity) * Cart[i].quantity;
                                total = parseInt(lish_orders.rows[j].price) * quantity
                            }
                            await lish_member.query(`INSERT INTO public.lish_orders_details
                                (order_number, shop_code, product_code, product_name, price, quantity, total,promotion_code,qty_promotion)
                                VALUES('${code}', '${newInfo.Shop}', '${lish_orders.rows[j].product_code}', '${lish_orders.rows[j].product_name}', ${parseInt(lish_orders.rows[j].price)}, ${quantity}, ${total},'${lish_orders.rows[j].promotion_code}',${Cart[i].quantity});`)
                        }
                    }
                    res.send({Message:'Success'})
                }
            })
            
        }else if(id == 2){
            //console.log(total);
            //console.log(req.body);
            req.session.Data = req.body.Data;
            var Data = req.session.Data
            var Shop_code = req.session.Data.Shop;
            let fname = Data.fname;
            let lname = Data.lname;
            let numberphone = Data.numberphone;
            let email = Data.email;
            let address = Data.address;
            let district = Data.district;
            let amphoe = Data.amphoe;
            let province = Data.province;
            let postcode = Data.postcode;
            let payment = Data.payment;
            let payment_status = Data.payment_status;
            let delivery_status = Data.delivery_status;
            let courier_code = Data.courier_code;
            let courier_tracking = Data.courier_tracking;
            let courier_booking = Data.courier_booking;
            let bank = Data.bank;
            let datetime = format(new Date,"yyyy-MM-dd HH:mm:ss")
            let check_count = await lish_member.query(`SELECT order_number from lish_orders where DATE(order_date) = DATE(now()) and shop_code = '${Shop_code || req.user.shop_code}' ORDER by id desc limit 1`)
            let shop_id = await lish_member.query(`SELECT id FROM setting_shop where shop_code = '${Shop_code}'`)
            //console.log(shop_id.rows[0].id);
            let yearsnow = format(new Date(),'yyyyMMdd')
            let shop = `${parseInt('0000')+parseInt(shop_id.rows[0].id)}`
            let tel = numberphone.slice(-3)
            let code_text = 0
            if(check_count.rows.length != 0){
                code_text = (check_count.rows[0].order_number).substring(14, 18)
                code_text = parseInt(code_text)+1
            }else{
                code_text = `0001`;
            }
            let code = `L${String(parseInt(shop)).padStart(4, '0')}-${yearsnow}${String(parseInt(code_text)).padStart(4, '0')}${tel}`
            Order_name = code
            await lish_member.query(`INSERT INTO public.lish_orders
            (order_number, first_name, last_name,tel, tel2, email, address, subdistrict, district, province, zipcode, payment, payment_status, delivery, delivery_status, courier_code, courier_tracking, courier_booking, bank, images, marketplace_tracking, social, reason,order_date, price, discount, freight, total, order_status,shop_code)
            VALUES('${code}',
            '${fname}', 
            '${lname}',
            '${numberphone}',
            '', 
            '${email}', 
            '${address}', 
            '${district}', 
            '${amphoe}',
            '${province}',
            '${postcode}',
            ${parseInt(payment)},
            ${parseInt(payment_status)},
            0,
            ${parseInt(delivery_status)},
            '${courier_code}',
            '${courier_tracking}',
            '${courier_booking}',
            ${parseInt(bank)},
            '',
            '', 
            '12',
            '',
            '${datetime}',
            ${total},
            0,
            0,
            ${total},
            2,
            '${Shop_code}');`)
            for(let i = 0 ; i < Cart.length ; i++){
                let lish_orders = await lish_member.query(`select * from lish_promotion_detail lpd left join lish_product_user lpu on lpd.product_code = lpu.product_code_user where lpd.promotion_code = '${Cart[i].id}' `)
                let quantity = 0
                let total = 0
                for(let j = 0 ; j < lish_orders.rows.length ; j++){
                    // console.log(Cart[i].id);
                    // console.log(lish_orders.rows[j].promotion_code);
                    if(lish_orders.rows[j].promotion_code == Cart[i].id){
                        quantity = parseInt(lish_orders.rows[j].quantity) * Cart[i].quantity;
                        total = parseInt(lish_orders.rows[j].price) * quantity
                    }
                    await lish_member.query(`INSERT INTO public.lish_orders_details
                        (order_number, shop_code, product_code, product_name, price, quantity, total,promotion_code,qty_promotion)
                        VALUES('${code}', '${Shop_code || req.user.shop_code}', '${lish_orders.rows[j].product_code}', '${lish_orders.rows[j].product_name}', ${parseInt(lish_orders.rows[j].price)}, ${quantity}, ${total},'${lish_orders.rows[j].promotion_code}',${Cart[i].quantity});`)
                }
            }
            res.send({Message:'Success'})
        }else if (id == 3){
            if(req.body.channel_response_desc != 'Existing OrderNo.' && req.body.channel_response_desc != 'Unable to authenticate cardholder' && req.body.channel_response_desc != 'Payment is cancelled.'){
                let name = data_credit.Name;
                let fnamelname = name.split(' ');
                await lish_member.query(`INSERT INTO public.lish_orders
                (order_number, first_name, last_name,tel, tel2, email, address, subdistrict, district, province, zipcode, payment, payment_status, delivery, delivery_status, courier_code, courier_tracking, courier_booking, bank, images, marketplace_tracking, social, reason,order_date, price, discount, freight, total, order_status,shop_code)
                VALUES('${Order_name}',
                '${fnamelname[0]}', 
                '${fnamelname[1]}',
                '${data_credit.NumberPhone}',
                '', 
                '${data_credit.Email}', 
                '${data_credit.Address}', 
                '${data_credit.district}', 
                '${data_credit.amphoe}',
                '${data_credit.province}',
                '${data_credit.postcode}',
                ${4},
                ${2},
                0,
                ${0},
                '0',
                '${0}',
                '${0}',
                ${0},
                '',
                '', 
                '12',
                '',
                'now()',
                ${total},
                0,
                0,
                ${total},
                1,
                '${data_credit.Shop}');`)
                for(let i = 0 ; i < cart_credit.length ; i++){
                    let lish_orders = await lish_member.query(`select * from lish_promotion_detail lpd left join lish_product_user lpu on lpd.product_code = lpu.product_code_user where lpd.promotion_code = '${cart_credit[i].id}' `)
                    let quantity = 0
                    let total = 0
                    for(let j = 0 ; j < lish_orders.rows.length ; j++){
                        if(lish_orders.rows[j].promotion_code == cart_credit[i].id){
                            quantity = parseInt(lish_orders.rows[j].quantity) * cart_credit[i].quantity;
                            total = parseInt(lish_orders.rows[j].price) * quantity
                        }
                        await lish_member.query(`INSERT INTO public.lish_orders_details
                            (order_number, shop_code, product_code, product_name, price, quantity, total,promotion_code,qty_promotion)
                            VALUES('${Order_name}', '${data_credit.Shop}', '${lish_orders.rows[j].product_code}', '${lish_orders.rows[j].product_name}', ${parseInt(lish_orders.rows[j].price)}, ${quantity}, ${total},'${lish_orders.rows[j].promotion_code}',${cart_credit[i].quantity});`)
                    }
                }
                res.redirect('../shop/ordersuccess3')
            }else{
                res.redirect(`../shop?shop=${data_credit.Shop}`);
            }
        }
        
    }
    async order_success(req,res){
        let Data = req.session.Data;
        let Cart = req.session.Cart;
        let id = req.params.id;
        let shop_code = req.query.shop;
        let token = await lish_member.query(`select * from setting_shop where shop_code = $1`,[shop_code])
        let str = ''
        if(token.rows.length != 0){
            if(token.rows[0].line_token != ' '){
                str = token.rows[0].line_token;
            }
        }
        if(id == 2){
            let delivery = 0
            // let img = await lish_member.query(`Select * from lish_orders where order_number = '${Order_name}'`)
            const url_line_notification = "https://notify-api.line.me/api/notify";
            request({
                method: 'POST',
                uri: url_line_notification,
                header: {
                    'Content-Type': 'multipart/form-data',
                },
                auth: {
                    bearer: `${str}`,
                },
                form: {
                    // message: `Hi, ${name} \n  สินค้า 1 ราคา $   บาท\n    สินค้า 2 ราคา $  บาท\n ราคารวม  บาท `
                    message: `\nเลขที่คำสั่งซื้อ : ${Order_name}\nชื่อลูกค้า : ${Data.fname} ${Data.lname}\nเบอร์โทร : ${Data.numberphone}\nที่อยู่ : ${Data.address} ${Data.district} ${Data.amphoe} ${Data.province} ${Data.postcode}\n\nรายการสินค้า :\n${display(Cart)} \nราคา : ${new Intl.NumberFormat().format(total)} บาท\nส่วนลดทั้งหมด : 0 \nค่าส่ง : ${delivery} บาท\nราคารวม: ${new Intl.NumberFormat().format((total+delivery))} บาท\nการชำระเงิน: เก็บเงินปลายทาง\nเมื่อ : ${checkDate()}`
                },
            }, (err, httpResponse, body) => {
                if (err) {
                    console.log(err)
                } else {
                    console.log(body)
                }
            });
            req.session.destroy();
            res.render('./shop/ordersuccess',{Shop_code: shop_code})
        }else if(id == 1){
            let delivery = 0
            let img = await lish_member.query(`Select * from lish_orders where order_number = '${Order_name}'`)
            const url_line_notification = "https://notify-api.line.me/api/notify";
            const imageFile = `http://128.199.100.251:1001/${JSON.parse(img.rows[0].images)[0].img}`
            request({
                method: 'POST',
                uri: url_line_notification,
                header: {
                    'Content-Type': 'multipart/form-data',
                },
                auth: {
                    bearer: `${str}`,
                },
                form: {
                    // message: `Hi, ${name} \n  สินค้า 1 ราคา $   บาท\n    สินค้า 2 ราคา $  บาท\n ราคารวม  บาท `
                    message: `\nเลขที่คำสั่งซื้อ : ${Order_name}\nชื่อลูกค้า : ${Data.Name}\nเบอร์โทร : ${Data.NumberPhone}\nที่อยู่ : ${Data.Address} ${Data.district} ${Data.amphoe} ${Data.province} ${Data.postcode}\n\nรายการสินค้า :\n${display(Cart)} \nราคา : ${new Intl.NumberFormat().format(total)} บาท\nส่วนลดทั้งหมด : 0 \nค่าส่ง : ${delivery} บาท\nราคารวม: ${new Intl.NumberFormat().format((total+delivery))} บาท\nการชำระเงิน: โอนเงิน\nเมื่อ : ${checkDate()} \n ลิงค์รูปภาพ : ${imageFile}`,
                    imageThumbnail: imageFile,
                    imageFullsize: imageFile
                },
            }, (err, httpResponse, body) => {
                if (err) {
                    console.log(err)
                } else {
                    console.log(body)
                }
            });
            req.session.destroy();
            res.render('./shop/ordersuccess',{Shop_code: shop_code})
        }else if(id == 3){
            let delivery = 0
            let token = await lish_member.query(`select * from setting_shop where shop_code = $1`,[data_credit.Shop])
            let str = ''
            if(token.rows.length != 0){
                if(token.rows[0].line_token != ' '){
                    str = token.rows[0].line_token;
                }
            }
            // let img = await lish_member.query(`Select * from lish_orders where order_number = '${Order_name}'`)
            const url_line_notification = "https://notify-api.line.me/api/notify";
            request({
                method: 'POST',
                uri: url_line_notification,
                header: {
                    'Content-Type': 'multipart/form-data',
                },
                auth: {
                    bearer: `${str}`,
                },
                form: {
                    // message: `Hi, ${name} \n  สินค้า 1 ราคา $   บาท\n    สินค้า 2 ราคา $  บาท\n ราคารวม  บาท `
                    message: `\nเลขที่คำสั่งซื้อ : ${Order_name}\nชื่อลูกค้า : ${data_credit.Name}\nเบอร์โทร : ${data_credit.NumberPhone}\nที่อยู่ : ${data_credit.Address} ${data_credit.district} ${data_credit.amphoe} ${data_credit.province} ${data_credit.postcode}\n\nรายการสินค้า :\n${display(cart_credit)} \nราคา : ${new Intl.NumberFormat().format(total)} บาท\nส่วนลดทั้งหมด : 0 \nค่าส่ง : 0 บาท\nราคารวม: ${new Intl.NumberFormat().format(total)} บาท\nการชำระเงิน: จ่ายด้วยบัตรเครดิต \nเมื่อ : ${checkDate()}`
                },
            }, (err, httpResponse, body) => {
                if (err) {
                    console.log(err)
                } else {
                    console.log(body)
                }
            });
            req.session.destroy();
            res.render('./shop/ordersuccess',{Shop_code: Data.Shop})
        }
        
    }
    returned(req,res){
        var {Count} = req.body;
        res.send({Count:Count})
    }
}
function generateDateOrder(Id,places){
    let zero 
    if(Id == 0 ){
        Id += 1;
        zero = places - Id.toString().length + 1;
    }else{
        zero = places - Id.toString().length + 1;
    }
    
    return Array(+(zero > 0 && zero)).join("0") + Id.toString();
}
function generateIdShop(Id,places){
    var zero = places - Id.toString().length + 1;
    return Array(+(zero > 0 && zero)).join("0") + Id.toString();
}
function generateDate(){
    let newDate = format(new Date(),'yyyy-MM-dd')
    newDate = newDate.split('-').join('');
    return newDate;
}
function display(cart){
    var item_name = []
    // console.log(cart);
    // console.log(cart);
    for(let i = 0 ; i < cart.length ; i++){
        item_name.push(`${i+1}. ${cart[i].name} จำนวน : ${cart[i].quantity} ชิ้น\n`)
    }
    
    // console.log(item_name.join(""));
    return item_name.join("")
} 
function checkDate() {
    let date_ob = new Date();
    let date = ("0" + date_ob.getDate()).slice(-2);
    // current month
    let month = ("0" + (date_ob.getMonth() + 1)).slice(-2);
    // current year
    let year = date_ob.getFullYear();
    // current hours
    let hours = date_ob.getHours();
    // current minutes
    let minutes = date_ob.getMinutes();
    // current seconds
    let seconds = date_ob.getSeconds();
    return `${date}-${month}-${year} เวลา ${hours}:${minutes}:${seconds}`
}
function Selection(data) {
    if(data.COD == 1) {
        return "เก็บเงินปลายทาง"
    }
    else if(data.BANK == 1) {
        return "การโอนชำระ"
    }
    else if(data.CREDIT == 1) {
        return "จ่ายผ่านบัตรเครดิต"
    }
    
}
