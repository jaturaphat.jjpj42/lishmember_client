const {format,getYear,addDays} = require("date-fns")
const crypto = require("crypto");
const { lish_member } = require("../../config/Database");
require('dotenv').config()

exports.paymentview = async (req,res) =>{
    try {
        let {brand,inv} = req.query;
        if(req.query.inv != null || req.query.inv != undefined || req.query.inv != ''){
            let session = req.session
            let data = session.data
            data.order_number = session.data.code
            console.log(data)
            
            // let data_price = await lish_member.query(`select price from lish_product where id = '${data.product_code}'`)

            var hmac = crypto.createHmac('sha256','QnmrnH6QE23N'); 
            // QnmrnH6QE23N
            // JT04
            let merchant_id = 'JT04';          //Get MerchantID when opening account with 2C2P
            let url = process.env.URL
            let payment_description =  `${data.code}`;
            let order_id = data.code;
        
            let currency = "764";
            var digit = '0000000000';
            var price = digit+'200'+'00';
            var totalPrice = price.substr(-12, 12);    
            let amount = totalPrice;   
                                            
            //Request information
            let version = "8.5";  
            // var ipp_interest_type = 'A';
            // var ipp_period_filter = "3"; 
            var payment_option = "ALL";//CC
            let payment_url = "https://t.2c2p.com/RedirectV3/payment";  
            // let resultUrl1 = `https://affiliate.awbg.co.th/order/thank?order_number=${order_id}`; 
            // let resultUrl1 = `https://affiliateuat.awbg.co.th/order/${orders[0].product_model}/thank`;    
            // let resultUrl1 = `https://affiliateuat.awbg.co.th/order/thank?order_number=${order_id}`; 
            // let resultUrl2 = `https://affiliateuat.awbg.co.th/api/v1/callback`; 
            let resultUrl1 = `${process.env.urlepayment}/epayment/thank?inv=${order_id}`;    
            let resultUrl2 = `${process.env.urlepayment}/api/callback`;  
                                    
            //Construct signature string
            var params = version+merchant_id+payment_description+order_id+currency+amount+resultUrl1+resultUrl2+payment_option; //+ipp_interest_type+ipp_period_filter , ipp_interest_type:ipp_interest_type, ipp_period_filter:ipp_period_filter

            var datahex = hmac.update(params);
            var gen_hmac= datahex.digest('hex');
            res.render('./epayment/payment',{data:data,version:version,merchant_id:merchant_id,currency:currency,resultUrl1:resultUrl1,resultUrl2:resultUrl2,hash_value:gen_hmac,payment_description:payment_description,order_id:order_id,amount:amount,payment_option:payment_option});
        }
    } catch (error) {
        console.log(error);
    }
}


exports.thankyouview = async (req,res) =>{
    try {
        // let {brand,inv} = req.query;
        // let databrand = await queryData(`SELECT * FROM oms_brand WHERE brand_code = '${brand}'`)
        res.redirect('./Product/linkLish/Thank')
    } catch (error) {
        console.log(error)
    }
}
