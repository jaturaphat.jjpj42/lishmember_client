const {format } = require("date-fns")
const {oneoms,whoms,lish_member} = require('../../config/Database')
const sessions = require('express-session');
const multer =require("multer")
const fs = require("fs")
const bcrypt= require('bcryptjs')
const Noti = require('../../config/LineNoti')

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

var storage1 = multer.diskStorage({
    destination: function (req, file, callback) {
    var dir = "./src/public/image/user";
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    callback(null, dir);
    },
    filename: function (req, file, callback) {
    let randomname = makeid(5)
    let name = file.originalname
    let name_data = name.substring(0,3)
    let datetime = format(new Date,"yyyyMMddHHmmss")
    callback(null, `${name_data}${randomname}-${datetime}.jpg`);
    },
});

var uploadpicturefields = multer({ storage: storage1 }).fields([{name:"file1"},{name:"file2"},{name:"file3"},{name:"file4"}]);

exports.user = class user{
    async view(req,res){
        let seller = req.params.seller
        let session = req.session
        await oneoms.query(`select * from users where username = '${seller}' and status = 1`,async (err,result)=>{
            if(err){
                console.log(err)
                res.redirect('/login')
            }else{
                console.log(result.rows)
                if(result.rows.length > 0){
                    let data = await lish_member.query(`select email from member_lish`)
                    session.user = seller
                    res.render('./register/Register' ,{data:data.rows})
                }else{
                    res.redirect('/login')
                }
            }
        })
    }

    profile(req,res){
        let {type} = req.body
        let session = req.session
        console.log(req.body)
        session.data = req.body
        let roles = 1
        if(type == 2){
            roles = 2
        }
        res.render('./register/Register2',{roles:roles})
    }

    profile_insert(req,res){
        uploadpicturefields(req,res,async function (err) {
            if(err){
                console.log(err)
            }else{
                let session = req.session
                let data = session.data
                console.log(session.data)
                session.data = Object.assign(req.body,data)
                session.data.image_id = '/image/user/' + req.files.file1[0].filename
                session.data.image_bookbank = '/image/user/' + req.files.file2[0].filename
                if(data.type == 2){
                    session.data.image_company = '/image/user/' + req.files.file3[0].filename
                }
                if(session.data.email == undefined){
                    if(session.user != undefined){
                        res.redirect('/register/member/'+session.user)
                    }else{
                        res.redirect('/register/member/seller')
                    }
                }else{
                    res.redirect('/register/confirm/')
                }
            }
        })
    }

    profile_confirm(req,res){
        let session = req.session
        console.log(session.user)
        if(session.data != undefined){
            res.render('./register/Register3')
        }else{
            res.redirect('/register/member/seller')
        }
    }

    async insert_data(req,res){
        let session = req.session
        let data = session.data
        let user = session.user
        let passHash = await bcrypt.hash(data.pass, 8)
        if(data.type == '1'){
            data.company_name = null
            data.company_tel = null
            data.image_company = null
            data.company_email = null
        }else{
            data.company_name = `'${data.company_name}'`
            data.company_tel = `'${data.company_tel}'`
            data.company_email = `'${data.company_email}'`
            data.image_company = `'${data.image_company}'`
        }
        let check_count = await lish_member.query(`SELECT max(shop_code) from setting_shop`)
        let yearsnow = format(new Date(),'yyMM')
        let code_text = `0`
        if(check_count.rows[0].max != null){
            code_text = (check_count.rows[0].max).substring(8)
            code_text = parseInt(code_text)+1
        }else{
            code_text = `001`;
        }
        let code = 'ML'+ yearsnow + (String(parseInt(code_text)).padStart(3, '0'))
        if(data.sent == 1){data.sent = 0}
        else{data.sent = 1}
        await lish_member.query(`INSERT INTO member_lish 
        (roles, status, first_name, last_name, email, tel, "password",
        con_password, address, subdistrict, district, province, zipcode,
        id_card, id_card_image, confirm_pdpa, book_bank_image, company_name,
        company_email, company_tel, company_image, seller ,shop_code,type_sent,created) 
        VALUES
        (0, 0, '${data.firstname}', '${data.lastname}', '${data.email}', '${data.tel}',
        '${passHash}', '${passHash}', '${data.address}', '${data.subdistrict}',
        '${data.district}', '${data.province}', '${data.zipcode}', '${data.id_card}', '${data.image_id}',
        1, '${data.image_bookbank}', ${data.company_name}, ${data.company_email}, ${data.company_tel}, ${data.image_company}, '${user}','${code}',${data.sent},now())`
        ,async (err,result)=>{
            if(err){
                console.log(err)
                res.send({Message:'เกิดข้อผิดพลาดติดต่อ Programmer'})
            }else{
                let msg = `\nระบบ Member\nชื่อ ${data.firstname + " " + data.lastname}\nเบอร์ : ${data.tel}\nSeller: ${user}`
                Noti(msg,`kPayLOdXnaSVfdjvixMB4d1oCWdGbNHyk4ce6xRo55M`,(err,reslut)=>{
                    if(err){
                        console.log(err)
                        res.send({Message:'Error'})
                    }else{
                        res.send({Message:'Success'})
                    }
                })
            }
        })
    }
}