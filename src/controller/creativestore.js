const {oneoms,whoms,lish_member} = require('../../config/Database')
const multer = require('multer');
const fs = require("fs");
const {format} = require("date-fns")

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}


var storage1 = multer.diskStorage({
    destination: function (req, file, callback) {
    var dir = "./src/public/image/creativestore";
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    callback(null, dir);
    },
    filename: function (req, file, callback) {
    let randomname = makeid(5);
    let name = file.originalname
    let name_data = name.substring(0,3)
    let datetime = format(new Date,"yyyyMMddHHmmss")
    callback(null, `${name_data}${randomname}-${datetime}.jpg`);
    },
});

let uploadfilesimage = multer({ storage: storage1 }).array("files");
exports.creativestore = class creativestore{
    async views(req,res){
        res.render('./creativestore/creativestore',{Shop_code:req.app.locals.shop})
    }
    async savedata(req,res){
        uploadfilesimage(req,res,async function(err){
            let div = req.body.div;
            let data = req.body.data;
            let count = req.body.count
            let arr = []
            let Photoslide = [];
            let Vdo = [];
            let Photo = []
            let SecondPhoto = []
            let Photolink = []
            let Photo2Link = []
            let Text = []
            let Line = []
            let FaceBook = []
            let Inbox = []
            let div_number = []
            let num = 1;
            if(count == 1){
                div_number.push({div:div})
                if(div === 'Photoslide'){
                    for(let i = 0 ; i < data.length ; i++){
                        let arr = data[i].split('@@')
                        let img = req.files.filter((element)=>element.originalname == arr[1])
                        Photoslide.push({div:arr[0],img:img[0].filename})
                    }
                }else if(div === 'Vdo'){
                    let arr = data.split('@@')
                    Vdo.push({div:arr[0],value:arr[1]})
                }else if(div === 'Photo'){
                    let arr = data.split('@@')
                    let img = req.files.filter((element)=>element.originalname == arr[1])
                    Photo.push({div:arr[0],img:img[0].filename})
                }else if(div === 'SecondPhoto'){
                    for(let i = 0 ; i < data.length ; i++){
                        let arr = data[i].split('@@')
                        let img = req.files.filter((element)=>element.originalname == arr[1])
                        SecondPhoto.push({div:arr[0],img:img[0].filename})
                    }
                }else if(div === 'Photolink'){
                    let arr = data.split('@@')
                    let img = req.files.filter((element)=>element.originalname == arr[1])
                    Photolink.push({div:arr[0],img:img[0].filename,link:arr[2]})
                }else if(div === 'Photo2Link'){
                    for(let i = 0 ; i < data.length ; i++){
                        let arr = data[i].split('@@')
                        let img = req.files.filter((element)=>element.originalname == arr[1])
                        Photo2Link.push({div:arr[0],img:img[0].filename,link:arr[2]})
                    }
                }else if(div === 'Text'){
                    let arr = data.split('@@')
                    let my_string = arr[1];
                    my_string = my_string.replace(/\n/g,'<br>')
                    Text.push({div:arr[0],value:my_string})
                }else if(div === 'Line'){
                    let arr = data.split('@@')
                    Line.push({div:arr[0],value:arr[1]})
                }else if(div === 'Facebook'){
                    let arr = data.split('@@')
                    FaceBook.push({div:arr[0],value:arr[1]})
                }else if(div === 'Inbox'){
                    let arr = data.split('@@')
                    Inbox.push({div:arr[0],value:arr[1]})
                }
                
            }else{
                for(let i = 0; i < count; i++){
                    div_number.push({div:div[i]})
                    for(let j = 0; j < data.length; j++){
                        let arr = data[j].split('@@');
                        if(arr[0] == `photoslide${i+1}`){
                            req.files?.filter((element)=>{
                                if(element.originalname == arr[1]){
                                    arr[1] = element.filename
                                }
                            })
                            Photoslide.push({div:arr[0],img:arr[1]})  
                        }else if(arr[0] == `video${i+1}`){
                            Vdo.push({div:arr[0],value:arr[1]})
                        }else if(arr[0] == `onlyphoto${i+1}`){
                            let img = req.files.filter((element)=>element.originalname == arr[1])
                            Photo.push({div:arr[0],img:img[0].filename})
                        }else if(arr[0] == `secondphoto${i+1}`){
                            let img = req.files.filter((element)=>element.originalname == arr[1])
                            SecondPhoto.push({div:arr[0],img:img[0].filename})
                        }else if(arr[0] == `Urlphoto${i+1}`){
                            let img = req.files.filter((element)=>element.originalname == arr[1])
                            Photolink.push({div:arr[0],img:img[0].filename,link:arr[2]})
                        }else if(arr[0] == `photosecondUrl${i+1}`){
                            let img = req.files.filter((element)=>element.originalname == arr[1])
                            Photo2Link.push({div:arr[0],img:img[0].filename,link:arr[2]})
                        }else if(arr[0] == `Message${i+1}`){
                            let my_string = arr[1];
                            my_string = my_string.replace(/\n/g,'<br>')
                            Text.push({div:arr[0],value:my_string})
                        }else if(arr[0] == `Line${i+1}`){
                            Line.push({div:arr[0],value:arr[1]})
                        }else if(arr[0] == `facebook${i+1}`){
                            FaceBook.push({div:arr[0],value:arr[1]})
                        }else if(arr[0] == `Send${i+1}`){
                            Inbox.push({div:arr[0],value:arr[1]})
                        }
                    }
                }
            }
            let div_count = JSON.stringify(div_number)
            let ps = JSON.stringify(Photoslide)
            let vdo = JSON.stringify(Vdo)
            let pt = JSON.stringify(Photo)
            let pt2 = JSON.stringify(SecondPhoto)
            let ptl = JSON.stringify(Photolink)
            let pt2l = JSON.stringify(Photo2Link)
            let Txt = JSON.stringify(Text)
            let Ln = JSON.stringify(Line)
            let Fb = JSON.stringify(FaceBook)
            let Ib = JSON.stringify(Inbox)
            let check = await lish_member.query(`select * from shop_creative where shop_code = $1` , [req.user.shop_code] )
            if(check.rows.length == 0){
                await lish_member.query('INSERT INTO public.shop_creative(shop_code, slide_photo, video, only_photo, second_photo, photo_link, second_photo_link, message, line_message, facebook, inbox_message, order_creative)VALUES($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12)', [req.user.shop_code,ps,vdo,pt,pt2,ptl,pt2l,Txt,Ln,Fb,Ib,div_count])
                res.send({Message:'Success'})
            }else{
                await lish_member.query('UPDATE public.shop_creative SET shop_code=$1, slide_photo=$2, video=$3, only_photo=$4, second_photo=$5, photo_link=$6, second_photo_link=$7, message=$8, line_message=$9, facebook=$10, inbox_message=$11, order_creative=$12 where shop_code = $13',[req.user.shop_code,ps,vdo,pt,pt2,ptl,pt2l,Txt,Ln,Fb,Ib,div_count,req.user.shop_code])
                res.send({Message:'Success'})
            }    
            
        })
    }

    async getdata(req,res){
        let {shop} = req.query
        let Data_creative = await lish_member.query(`select * from shop_creative where shop_code = '${shop}'`)
        res.send({creative:Data_creative.rows})
    }
}