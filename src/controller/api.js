const { lish_member,select_lish } = require('../../config/Database')
const Modelapi = require('../model/api')
const multer =require("multer")
const crypto = require("crypto");
const PDFDocument = require("pdfmake-thai");
// const PDFDocument = require('pdfkit');
const fs = require("fs")
const {format} = require("date-fns")
var fonts = {
    THSarabunNew: {
        normal: './src/public/font_pdf/Sarabun/Sarabun-Regular.ttf',
        bold: './src/public/font_pdf/Sarabun/Sarabun-Medium.ttf',
        italics: './src/public/font_pdf/Sarabun/Sarabun-Italic.ttf',
        bolditalics: './src/public/font_pdf/Sarabun/Sarabun-MediumItalic.ttf'
    },
    Roboto: {
        normal: './src/public/font_pdf/Roboto/Roboto-Regular.ttf',
        bold: './src/public/font_pdf/Roboto/Roboto-Medium.ttf',
        italics: './src/public/font_pdf/Roboto/Roboto-Italic.ttf',
        bolditalics: './src/public/font_pdf/Roboto/Roboto-MediumItalic.ttf'
    }
};
function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

var storage1 = multer.diskStorage({
    destination: function (req, file, callback) {
    var dir = "./src/public/image/product";
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    callback(null, dir);
    },
    filename: function (req, file, callback) {
    let randomname = makeid(5)
    let name = file.originalname
    let name_data = name.substring(0,3)
    let datetime = format(new Date,"yyyyMMddHHmmss")
    callback(null, `${name_data}${randomname}-${datetime}.jpg`);
    },
});

var uploadpicturefields = multer({ storage: storage1 }).fields([{name:"file1"},{name:"file2"},{name:"file3"},{name:"file4"}]);

exports.api = class api{

    async bank(req,res){
        let bank_lish = new Modelapi.apimodel()
        let bank = await bank_lish.bank()
        console.log(bank.rows)
        res.send(bank.rows)
    }

    async transport(req,res){
        let transport_lish = new Modelapi.apimodel()
        let transport = await transport_lish.transport()
        console.log(transport.rows)
        res.send(transport.rows)
    }

    async check_name(req,res){
        let word = req.body.word
        await lish_member.query(`select count(*) from setting_shop where shop_name like '%${word}%'`,async (err,result)=>{
            if(err){
                console.log(err)
                res.send({Message:'พบข้อผิดพลาดในการตรวจสอบชื่อ'})
            }else{
                if(result.rows[0].count > 0){
                    res.send({Message:'Have'})
                }else{
                    res.send({Message:'Success'})
                }
            }
        })
    }

    async insert_order(req,res){
        uploadpicturefields(req,res,async function (err) {
            if(err){
                console.log(err)
            }else{
                let session = req.session
                let data = session.data
                // console.log(req.body)
                let member = await select_lish(`select * from setting_shop ss left join member_lish ml on ss.shop_code = ml.shop_code where ss.shop_code = '${data.shop_code}'`)
                let product = await select_lish(`select * from lish_product lp where lp.id = ${data.product_code}`)
                let address = {}
                if(data.type == 2){
                    address = {
                        subdistrict:data.subdistrict,
                        district:data.district,
                        province:data.province,
                        zipcode:data.zipcode,
                        address:data.address,
                    }
                }else{
                    address ={
                        subdistrict:member[0].subdistrict,
                        district:member[0].district,
                        province:member[0].province,
                        zipcode:member[0].zipcode,
                        address:member[0].address,
                    }
                }
                let img
                let order_status = 0
                if(data.option == 1){
                    img = `'${req.files.filename}'`
                    order_status = 1
                }else{
                    img = null
                    order_status = 1
                }
                await lish_member.query(`INSERT INTO member_order 
                (order_number, first_name, last_name, tel, email, address, subdistrict, district, province, zipcode
                , payment, payment_status, bank, images, shop_code, order_status ,order_date) 
                VALUES('${data.code}', '${member[0].first_name}', '${member[0].last_name}', '${member[0].tel}', '${member[0].email}'
                , '${address.address}', '${address.subdistrict}', '${address.district}', '${address.province}', '${address.zipcode}'
                , ${data.option}, 0, 0
                , ${img}, '${data.shop_code}', ${order_status} , Date(now()))`,async (err,result)=>{
                    if(err){
                        console.log(err)
                        res.send({Message:'Error_1'})
                    }else{
                        await lish_member.query(`INSERT INTO member_order_detail 
                        (order_number, shop_code, product_code_oms, quantity, price, order_date ,product_code_wh) 
                        VALUES('${data.code}', '${data.shop_code}', '${product[0].product_code_oms}', 1, ${product[0].price}, DATE(now()), '${product[0].product_code_wh}');
                        `,async (err,element)=>{
                            if(err){
                                console.log(err)
                                res.send({Message:'Error_2'})
                            }else{
                                if(session.data.option = 2){
                                    res.redirect('/api/linkLish/Thank')
                                }else{
                                    res.send({Message:'Success'})
                                }
                            }
                        })
                    }
                })
            }
        }) 
    }

    async view_Thank(req,res){
        let session = req.session
        let data_session = session.data
        // console.log(data_session)
        let id = data_session.code
        let product = data_session.product_code
        let member = await lish_member.query(`select * from member_order where order_number = '${id}'`)
        let data = await lish_member.query(`select *,lp.id as id,mr.roles_name from lish_product lp left join member_roles mr on lp.id_roles = mr.id where lp.id = ${product}`)
        res.render('./product/linklishthankyou',{Product:data.rows[0],code:id,member:member.rows[0]})
    }

    async promotion_list_detail(req,res){
        const ModelQuery = new Modelapi.apimodel()
        const prmotion_detail = await ModelQuery.promotion_detail()
        res.send(prmotion_detail.rows)
    }

    async pdf(req,res){
        var printer = new PDFDocument(fonts);
        var fs = require('fs');
        var docDefinition = {
            content: [],
            pageSize: {height: 425.25, width: 283.5},
            pageMargins: [ 5, 5, 5, 5],
            styles: {
                header: {
                fontSize: 12,
                }
            },
            defaultStyle: {
            font: 'THSarabunNew'
            }
        };
        // let order_number = req.body.order_number
        let order_number = ['L0033-202302280002546','L0033-202303010001546']
        // let data = await lish_member.query(`select *,concat(tel,' ',address,' ',subdistrict,' ',district,' ',province,' ',zipcode) as address from lish_orders where shop_code = '${req.user.shop_code}' and order_number = '${order_number}'`)
        let address = await lish_member.query(`select concat(shop_name ,' ',phone,' ',address,' ',subdistrict,' ',district,' ',province,' ',zipcode) as address from setting_shop where shop_code = '${req.user.shop_code}'`)

        for(let main = 0;main < order_number.length ;main++){
            let data = await lish_member.query(`select *,concat(tel,' ',address,' ',subdistrict,' ',district,' ',province,' ',zipcode) as address from lish_orders where shop_code = '${req.user.shop_code}' and order_number = '${order_number[main]}'`)
            let order_details = await lish_member.query(`select distinct lpd.promotion_code, lp.promotion_name,lpd.qty_promotion from lish_orders_details lpd left join lish_promotion lp on lpd.promotion_code = lp.promotion_code where lpd.order_number = '${order_number[main]}'`)
            let address_customer = `${data.rows[0].first_name} ${data.rows[0].last_name} ${data.rows[0].address}`
            let payment = `ไม่ต้องเก็บเงิน`
            if(data.rows[0].payment == 1){
                payment = `เก็บเงินปลายทาง`
                data.rows[0].total += ' บาท'
            }else{
                data.rows[0].total = `-`
            }
            docDefinition.content.push({image:'./src/public/assets/img/logo-black-01.png',width:40,alignment:'center',margin:[0,10,0,0]})
            docDefinition.content.push({
                style: 'tableExample',margin: [ 0, 0, 0, 5 ],
                table: {
                    widths: ['30%', '10%', '25%', '35%'],
                    heights: [20,20,10,10],
                    headerRows: 1,
                    body: [
                        [
                            {
                                text: 'ผู้ส่ง : \n'+address.rows[0].address,
                                colSpan: 3,
                                rowSpan:2,
                                bold: true,
                                fontSize: 9
                            },{},{},
                            {
                                text: 'Phone',
                                alignment: 'center',
                                rowSpan:[1,2],
                                fontSize: 10,
                                bold: true,
                            },
                        ],
                        [
                            {},{},{},{
                                text: '0897654321',
                                alignment: 'center',
                                colSpan:1,
                                rowSpan:1,
                                fontSize: 12,
                                bold: true,
                                margin:[10,0,0,0]
                            }
                        ],
                        [
                            {
                                text: 'ผู้รับ : '+address_customer,
                                colSpan: 3,
                                rowSpan:2,
                                bold: true,
                                fontSize: 9
                            },{},{},{
                                text: payment,
                                alignment: 'center',
                                colSpan:1,
                                rowSpan:1,
                                fontSize: 10,
                            }
                        ],
                        [
                            {},{},{},{text: data.rows[0].total ,alignment:'center'}
                        ],
                        [
                            {
                                text: data.rows[0].order_number,
                                alignment:'center',
                                colSpan:3,
                                rowSpan:2,
                                fontSize: 14,
                                bold: true,
                                margin: [0, 8, 0, 0]
                            }, 
                            { 
                            },
                            {
                            },
                            {
                                text: 'ZIPCODE',
                                alignment:'center',
                                colSpan: 1,
                                rowSpan:[2,3],
                                fontSize: 8,
                                bold: true,
                                margin:[0,0,0,0]
                            }
                        ],
                        [
                            {
                            }, 
                            { 
                            },
                            {
                            },
                            {   
                                text: data.rows[0].zipcode,
                                alignment:'center',
                                colSpan: 1,
                                rowSpan:1,
                                fontSize: 18,
                                bold: true,
                                margin:[0,-2,0,0]
                            },
                        ],
                    ]
                }
            })
            docDefinition.content.push({image:'./src/public/assets/img/cut.jpg',width:280.5})
            let data_details =[[{text:`#`,fontSize: 7,alignment:'center'},
            {text:'รหัสสินค้า',alignment:'center',fontSize: 7},
            {text:`ชื่อสินค้า`,alignment:'center',fontSize: 7},
            {text:'QTY',alignment:'center',fontSize: 7}]]
            let i = 1
            let total = 0
            order_details.rows.forEach((element)=>{
                data_details.push([{text:i,alignment:'center',fontSize: 7}
                    ,{text:element.promotion_code,alignment:'center',fontSize: 7}
                    ,{text:element.promotion_name,fontSize: 7}
                    ,{text:element.qty_promotion,alignment:'center',fontSize: 7}])
                i++
                total += parseInt(element.qty_promotion)
            })
            docDefinition.content.push({
                style: 'detail',margin:[ 0,2,0,2 ],
                layout: {
                    hLineWidth: function (i, node) {
                        return (i === 1 || i === 0 || i === data_details.length) ? 1 : 0;
                    },
                    vLineWidth: function (i, node) {
                    return (i === 3 || i === 4 || i === 0) ? 1 : 0;
                    }
                },
                table:{
                    widths: ['5%','15%','70%','10%'],
                    heights: 10,
                    headerRows:1,
                    body: data_details
                }
            })
            if((main+1) != order_number.length){
                docDefinition.content.push({
                    style: 'tableExample',
                    table: {
                        widths: [23,23],
                        heights: 'auto',
                        body: [
                            [{text:'รวม',alignment:'center',fontSize: 14}, {text:total,alignment:'center',fontSize: 14}],
                        ]
                    },margin: [208,0],pageBreak: `after`
                })
            }else{
                docDefinition.content.push({
                    style: 'tableExample',
                    table: {
                        widths: [23,23],
                        heights: 'auto',
                        body: [
                            [{text:'รวม',alignment:'center',fontSize: 14}, {text:total,alignment:'center',fontSize: 14}],
                        ]
                    },margin: [208,0]
                })
            }
            
        }

        var options = {
        }

        var pdfDoc = printer.createPdfKitDocument(docDefinition, options);
        pdfDoc.pipe(res);
        pdfDoc.end();
    }

}
exports.api_shop = class api_shop{
    async insert_db(req,res){
        if(req.body.channel_response_desc != 'Existing OrderNo.' && req.body.channel_response_desc != 'Unable to authenticate cardholder' && req.body.channel_response_desc != 'Payment is cancelled.'){
            
        }else{
            res.redirect('../shop/confirm_order');
        }
        // res.redirect('./ordersuccess');
    }
}