const {format } = require("date-fns")
const {oneoms,whoms,lish_member} = require('../../config/Database')
const sessions = require('express-session');
const multer =require("multer")
const fs = require("fs")
const Modelapi = require('../model/api')

function makeid(length) {
    var result           = '';
    var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for ( var i = 0; i < length; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
}

var storage1 = multer.diskStorage({
    destination: function (req, file, callback) {
    var dir = "./src/public/image/setting";
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    callback(null, dir);
    },
    filename: function (req, file, callback) {
    let randomname = makeid(5)
    let name = file.originalname
    let name_data = name.substring(0,3)
    let datetime = format(new Date,"yyyyMMddHHmmss")
    callback(null, `${name_data}${randomname}-${datetime}.jpg`);
    },
});

var storage = multer.diskStorage({
    destination: function (req, file, callback) {
    var dir = "./src/public/image/setting/channel";
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    callback(null, dir);
    },
    filename: function (req, file, callback) {
    let randomname = makeid(5)
    let name = file.originalname
    let name_data = name.substring(0,3)
    let datetime = format(new Date,"yyyyMMddHHmmss")
    callback(null, `${name_data}${randomname}-${datetime}.jpg`);
    },
});

var uploadpicturefields = multer({ storage: storage1 }).fields([{name:"file1"},{name:"file2"},{name:"file3"},{name:"file4"}]);
let uploadfilesimage = multer({ storage: storage }).array("files");

exports.setting = class setting{

    async views(req,res){
        let bank_lish = new Modelapi.apimodel()
        let bank = await bank_lish.bank()
        let transport = await bank_lish.transport()
        let id = {id:0}
        res.render('./setting/Setting',{bank:bank.rows,transport:transport.rows,data:id,Bank:[],delivery:[],api:[]})
    }

    async views_update(req,res){
        let bank_lish = new Modelapi.apimodel()
        let bank = await bank_lish.bank()
        let transport = await bank_lish.transport()
        let shop = req.params.shop
        let data = await lish_member.query(`select * from setting_shop ss where ss.shop_code = '${shop}'`)
        let Bank = await lish_member.query(`select * from setting_shop_transfer sf left join setting_bank sb on sf.bank = sb.bank_code where sf.shop_code = '${shop}'`)
        let delivery = await lish_member.query(`select * from setting_shop_transport st left join setting_delivery sd on st.transport_id = sd.delivery_code where st.shop_code = '${shop}'`)
        let api = await lish_member.query(`select * from setting_api sa where sa.shop_code = '${shop}'`)
        res.render('./setting/Setting',{bank:bank.rows,transport:transport.rows,data:data.rows[0],Bank:Bank.rows,delivery:delivery.rows,api:api.rows[0]})
    }

    async add_shop(req,res){
        uploadpicturefields(req,res,async function (err) {
            if(err){
                console.log(err)
            }else{
                let data = req.body
                let shop_Line_tk = ''
                let img = []
                let code = req.user.shop_code
                req.body.shop_Line_tk == '' ? shop_Line_tk = null : shop_Line_tk = `'${req.body.shop_Line_tk}'`
                await lish_member.query(`INSERT INTO setting_shop 
                (status, shop_name, email, phone, line_token, image_profile, image_bg,
                address, subdistrict, district, province, zipcode, status_cod, status_transfer, status_credit,"user",shop_code)
                VALUES(1, '${data.shop_name}', '${data.shop_email}', '${data.shop_phone}', ${shop_Line_tk},
                '/image/setting/${req.files.file1[0].filename}', '/image/setting/${req.files.file2[0].filename}',
                '${data.address}', '${data.subdistrict}', '${data.district}', '${data.province}', '${data.zipcode}',
                ${data.COD}, ${data.BANK}, ${data.CREDIT},'${data.user}','${code}');
                `,async (err,reslut)=>{
                    if(err){
                        console.log(err)
                        console.log('Insert error shop')
                        res.send({Message:'Error'})
                    }else{
                        await lish_member.query(`INSERT INTO setting_api (line_api, fb_api, ig_api, tt_api,shop_code) VALUES('${data.Line}',
                        '${data.FB}','${data.IG}', '${data.TT}','${code}');
                        `,async (err2,element)=>{
                            if(err2){
                                console.log(err2)
                                console.log('Insert error 2')
                                res.send({Message:'Error'})
                            }else{
                                let account_id = data.account_id
                                let account_name = data.account_name
                                let bank = data.bank
                                let update_user = await lish_member.query(`update member_lish set shop_code = '${code}' where email = '${data.user}'`)
                                let product = await lish_member.query(`select * from lish_product_user`)
                                let promotion = await lish_member.query(`select * from lish_promotion`)
                                let channel = await lish_member.query(`select * from lish_channel where shop_code = 'Admin'`)
                                promotion.rows.forEach(async element=>{
                                    let insert_shop = await lish_member.query(`insert into shop_promotion (promotion_code,promotion_name,status,shop_code) 
                                    values ('${element.promotion_code}', '${element.promotion_name}', 0 ,'${code}')`)
                                })
                                product.rows.forEach(async element=>{
                                    let insert_shop = await lish_member.query(`insert into shop_product (product_code,product_name,quantity,status,shop_code) 
                                    values ('${element.product_code_user}', '${element.product_name}', 0 ,0,'${code}')`)
                                })
                                channel.rows.forEach(async element=>{
                                    let insert_shop = await lish_member.query(`INSERT INTO shop_channel 
                                    (channel_name, channel_id, shop_code, status_channel, created_at, open_at, close_at, remak) 
                                    VALUES('${element.channel_name}', ${element.id}, '${req.user.shop_code}', 0, now(), null, null, null);`)
                                })
                                if(parseInt(data.account_count) == 1){
                                    await lish_member.query(`INSERT INTO setting_shop_transfer 
                                    (bank, bank_id, account_name, shop_code) 
                                    VALUES('${bank}', '${account_id}', '${account_name}', '${code}');`)
                                }else if(parseInt(data.account_count) >= 1){
                                    for(let i = 0;i<account_id.length;i++){
                                        await lish_member.query(`INSERT INTO setting_shop_transfer 
                                        (bank, bank_id, account_name, shop_code) 
                                        VALUES('${bank[i]}', '${account_id[i]}', '${account_name[i]}', '${code}');
                                        `)
                                    }
                                }
                                if(parseInt(data.transport_count) == 1){
                                    await lish_member.query(`INSERT INTO setting_shop_transport 
                                    (transport_id, shop_code) 
                                    VALUES('${data.transport}', '${code}');
                                    `)
                                    req.user.shop_code = code
                                    res.send({Message:'Success'})
                                }else if(parseInt(data.transport_count) >= 1){
                                    for(let j = 0;j<data.transport.length;j++){
                                        await lish_member.query(`INSERT INTO setting_shop_transport 
                                        (transport_id, shop_code) 
                                        VALUES('${data.transport[j]}', '${code}');
                                        `)
                                    }
                                    req.user.shop_code = code
                                    res.send({Message:'Success'})
                                }
                            }
                        })
                    }
                })
            }
        })
    }

    async update_shop(req,res){
        uploadpicturefields(req,res,async function (err) {
            if(err){
                console.log(err)
            }else{
                console.log(req.body)
                let data = req.body
                let shop_Line_tk = ''
                let img = []
                let shop_img = await lish_member.query(`select * from setting_shop where id = '${data.id_shop}'`)
                let code = shop_img.rows[0].shop_code
                img.push(shop_img.rows[0].image_profile)
                img.push(shop_img.rows[0].image_bg)
                req.body.shop_Line_tk == '' ? shop_Line_tk = null : shop_Line_tk = `'${req.body.shop_Line_tk}'`
                if(req.files != undefined){
                    if(req.files.file1 != undefined){
                        img[0] = `/image/setting/${req.files.file1[0].filename}`
                    }
                    if(req.files.file2 != undefined){
                        img[1] = `/image/setting/${req.files.file2[0].filename}`
                    }
                }
                await lish_member.query(`update setting_shop set
                shop_name = '${data.shop_name}', email ='${data.shop_email}', phone ='${data.shop_phone}'
                , line_token = ${shop_Line_tk}, image_profile='${img[0]}', image_bg='${img[1]}',
                address='${data.address}', subdistrict='${data.subdistrict}', district='${data.district}',
                province='${data.province}', zipcode='${data.zipcode}', status_cod=${data.COD}, status_transfer=${data.BANK},
                status_credit=${data.CREDIT} where id = '${data.id_shop}'
                `,async (err,reslut)=>{
                    if(err){
                        console.log(err)
                        console.log('Insert error shop')
                        res.send({Message:'Error'})
                    }else{
                        let detete_api = await lish_member.query(`delete from setting_api where shop_code = '${code}'`)
                        await lish_member.query(`INSERT INTO setting_api (line_api, fb_api, ig_api, tt_api,shop_code) VALUES('${data.Line}',
                        '${data.FB}','${data.IG}', '${data.TT}','${code}');
                        `,async (err2,element)=>{
                            if(err2){
                                console.log(err2)
                                console.log('Insert error 2')
                                res.send({Message:'Error'})
                            }else{
                                let account_id = data.account_id
                                let account_name = data.account_name
                                let bank = data.bank
                                let detete_Bank = await lish_member.query(`delete from setting_shop_transfer where shop_code = '${code}'`)
                                let detete_delivery = await lish_member.query(`delete from setting_shop_transport where shop_code = '${code}'`)
                                if(parseInt(data.account_count) == 1){
                                    await lish_member.query(`INSERT INTO setting_shop_transfer 
                                    (bank, bank_id, account_name, shop_code) 
                                    VALUES('${bank}', '${account_id}', '${account_name}', '${code}');`)
                                }else if(parseInt(data.account_count) >= 1){
                                    for(let i = 0;i<account_id.length;i++){
                                        await lish_member.query(`INSERT INTO setting_shop_transfer 
                                        (bank, bank_id, account_name, shop_code) 
                                        VALUES('${bank[i]}', '${account_id[i]}', '${account_name[i]}', '${code}');
                                        `)
                                    }
                                }
                                if(parseInt(data.transport_count) == 1){
                                    await lish_member.query(`INSERT INTO setting_shop_transport 
                                    (transport_id, shop_code) 
                                    VALUES('${data.transport}', '${code}');
                                    `)
                                    res.send({Message:'Success'})
                                }else if(parseInt(data.transport_count) >= 1){
                                    for(let j = 0;j<data.transport.length;j++){
                                        await lish_member.query(`INSERT INTO setting_shop_transport 
                                        (transport_id, shop_code) 
                                        VALUES('${data.transport[j]}', '${code}');
                                        `)
                                    }
                                    res.send({Message:'Success'})
                                }
                            }
                        })
                    }
                })
            }
        })
    }

    async viewsChannel(req,res){
        let channel = await lish_member.query(`select *,sc.shop_code as shop_code,lc.shop_code as created from shop_channel sc left join lish_channel lc on sc.channel_id = lc.id where sc.shop_code = '${req.user.shop_code}' and sc.on_delete = 'N' and lc.status = 1`)
        res.render('./setting/Channel' ,{Channel:channel.rows})
    }

    async Addchannel(req,res){
        uploadfilesimage(req,res,async function (err) {
            if(err){
                console.log(err)
            }else{
                let {text} = req.body
                await lish_member.query(`INSERT INTO lish_channel (channel_name,status,shop_code,created_at,image,on_delete) VALUES('${text}',1,'${req.user.shop_code}',now(),'http://128.199.100.251:1001/image/setting/channel/${req.files[0].filename}')`,async (err,result)=>{
                    if(err){
                        console.log(err)
                        res.send({Message:'Error'})
                    }else{
                        let data = await lish_member.query(`select * from lish_channel lc where lc.channel_name = '${text}'`)
                        let shop_insert = await lish_member.query(`INSERT INTO shop_channel (channel_name, channel_id, shop_code, status_channel, created_at, open_at, close_at, remak,on_delete) VALUES('${text}', '${data.rows[0].id}', '${req.user.shop_code}', 0, now(), null, null, null);`)
                        res.send({Message:'Success'})
                    }
                })
            }
        })
    }

    async Updatechannel(req,res){
        uploadfilesimage(req,res,async function (err) {
            if(err){
                console.log(err)
            }else{
                let img = ``
                let {text,id} = req.body
                if(req.files.length == 0){
                    let img_old = await lish_member.query(`select image from lish_channel where id = ${id}`)
                    img += img_old.rows[0].image
                }else{
                    img += `http://128.199.100.251:1001/image/setting/channel/${req.files[0].filename}`
                }
                await lish_member.query(`update lish_channel set channel_name='${text}',status=0,shop_code='${req.user.shop_code}',image='${img}',on_delete='N' where id = ${id}`,async (err,result)=>{
                    if(err){
                        console.log(err)
                        res.send({Message:'Error'})
                    }else{
                        let shop_insert = await lish_member.query(`UPDATE shop_channel SET channel_name='${text}', status_channel=0, close_at=now(), remak='เปลี่ยนชื่อช่องทาง',on_delete='N' where channel_id = '${id}';`)
                        res.send({Message:'Success'})
                    }
                })
            }
        })
    }

    async Deletechannel(req,res){
        let id = req.body.id
        await lish_member.query(`update lish_channel set status=0,on_delete='Y' where id = ${id}`,async (err,result)=>{
            if(err){
                console.log(err)
                res.send({Message:'Error'})
            }else{
                let delete_shop = await lish_member.query(`update shop_channel set status=0,on_delete='Y' where channel_id = ${id}`)
                res.send({Message:'Success'})
            }
        })
    }

    async Changechannel(req,res){
        let {id} = req.body
        await lish_member.query(`select * from shop_channel sc where channel_id = ${id} and shop_code = '${req.user.shop_code}'`,async (err,result)=>{
            if(err){
                console.log(err)
                res.send({Message:'Error'})
            }else{
                if(result.rows.length != 0){
                    if(result.rows[0].status_channel == 0){
                        await lish_member.query(`update shop_channel set status_channel=1,open_at=DATE(now()) where channel_id = ${id} and shop_code = '${req.user.shop_code}'`)
                        res.send({Message:'Success'})
                    }else{
                        await lish_member.query(`update shop_channel set status_channel=0,close_at=DATE(now()),remak='ปิดโดยร้าน' where channel_id = ${id} and shop_code = '${req.user.shop_code}'`)
                        res.send({Message:'Success'})
                    }
                }
            }
        })
    }
}