<%-include('./partials/header')%>
<%-include('./partials/navbar')%>
<%-include('./partials/slidebar')%>
<%-include('./partials/scripts')%>


INSERT INTO public.lish_payment_type
(payment_name, status)
VALUES('เก็บเงินปลายทาง (COD)', 0);
INSERT INTO public.lish_payment_type
(payment_name, status)
VALUES('โอนเงิน', 0);
INSERT INTO public.lish_payment_type
(payment_name, status)
VALUES('พกง', 0);
INSERT INTO public.lish_payment_type
(payment_name, status)
VALUES('บัตรเครดิต', 0);
INSERT INTO public.lish_payment_type
(payment_name, status)
VALUES('เงินสด', 0);
INSERT INTO public.lish_payment_type
(payment_name, status)
VALUES('ฟรี', 0);
INSERT INTO public.lish_payment_type
(payment_name, status)
VALUES('Facebook', 0);
INSERT INTO public.lish_payment_type
(payment_name, status)
VALUES('Line Pay', 0);
INSERT INTO public.lish_payment_type
(payment_name, status)
VALUES('Paypal', 0);
INSERT INTO public.lish_payment_type
(payment_name, status)
VALUES('เช็ค', 0);
INSERT INTO public.lish_payment_type
(payment_name, status)
VALUES('2C2P', 0);
INSERT INTO public.lish_payment_type
(payment_name, status)
VALUES('Marketplace', 0);
